syntax on
colorscheme desert
set background=dark
hi comment ctermfg=red

set nocompatible
set confirm

set autoindent
set cindent
set smartindent
set tabstop=4
set softtabstop=2
set shiftwidth=4

set expandtab
set smarttab

set cino=g2,h2

set number
set history=100

set nobackup
set noswapfile

set ignorecase
set hlsearch
set incsearch

set statusline=%F%m%r%h%w\[TYPE=%Y]\[POS=%l,%v][%p%%]\%{strftime(\"%d/%m/%y\ -\ %H:%M\")}
set laststatus=2

filetype on
filetype plugin on
filetype indent on

set report=0

set showmatch
set matchtime=5
set scrolloff=3

au BufNewFile,BufRead *.cu set ft=cuda

au BufNewFile,BufRead *.cl set ft=opencl
