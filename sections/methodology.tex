\chapter{Methodology}
\label{ch:methodology}
This chapter describes the experimental methodology, including the performance simulator for integrated CPU-GPU heterogeneous processors and the benchmarks.
Note that the system configurations of mainstream commercial processors such as CPU core frequency and number of GPU SMs have evolved over recent years.
Therefore certain configuration parameters used in the experiments have been adjusted and hence separately detailed in each project to match its contemporary commercial processor released by AMD.

\section{Performance Simulator}
\label{sec:perf_simulator}
In this thesis, we target single-chip heterogeneous processors that the CPU and the GPU are integrated in a single chip and share one or more memory controllers (MCs) to service memory requests from both the CPU and the GPU.
Because this leads to memory access contentions, it is critical to build a detailed cycle-level simulator to accurately model the effects of the shared memory channel between the CPU and the GPU, as well as the memory access contention.
In developing this infrastructure and considering the current heterogeneous computing model, we recognize that we are mainly interested in the interactions between the CPU and the GPU at the MCs, from the performance modeling perspective.
This is because the CPU and the GPU are running independent threads and hence do not interact directly with each other except for the interactions through the off-chip main memory.
Note that AMD APUs currently do not share a last-level cache between the CPU and the GPU.

Our integrated simulator infrastructure has been developed based on two widely-used simulators: gem5 \cite{gem5} and GPGPU-Sim \cite{gpgpusim} for modeling the CPU and the GPU, respectively.
The gem5 and GPGPU-Sim run as two separate processes and communicates through the shared memory in the Linux operating system, as shown in Figure~\ref{subfig:simulator_overview}.
\begin{figure}[!b]
    \centering
    \subfloat[Overview]{
        \centering
        \includegraphics[width=3.6in,clip=false,trim=0 0 0 0]{figures/simulator_overview.pdf}
        \label{subfig:simulator_overview}
    }
    \qquad
    \subfloat[Shared memory channel]{
        \centering
        \includegraphics[width=3.6in,clip=false,trim=0 0 0 0]{figures/simulator_mc.pdf}
        \label{subfig:simulator_mc}
    }
    \caption{Implementation of integrated gem5+GPGPU-Sim simulator}
    \label{fig:simulator}
\end{figure}
We use the gem5 to model the out-of-order cores and the cache hierarchy of the CPU, and the GPGPU-Sim to model the streaming multiprocessors (SMs) \cite{gpuera} and on-chip interconnects of the GPU.
We remove the GPGPU-Sim's MC and DRAM model, leaving only a set of request and response queues per MC; GPGPU-Sim communicates with the memory system of gem5 to service its memory accesses through shared memory structures.
There are two requirements for the integrated gem5 and GPGPU-Sim simulator to correctly model the CPU-GPU heterogeneous processor's behavior.
The first is to ensure the two processes to operate in lock-step, and the second is to provide a mechanism for GPGPU-Sim to place its requests to gem5's memory system on a cache miss, and receive correctly-timed responses from gem5's memory system.

In order to guarantee lock-step execution, the gem5 provides periodic SM-blocking ticks and memory-blocking ticks to the GPGPU-Sim through shared memory structures, as shown in Figure~\ref{subfig:simulator_overview}. The gem5 issues one blocking tick for all SMs in GPGPU-Sim, and one memory tick per MC in GPGPU-Sim.
The gem5 triggers SMs or MCs in GPGPU-Sim by setting a flag in shared memory structures; the gem5 then blocks itself until GPGPU-Sim completes the execution of a SM or MC cycle and resets the flag to resume gem5.

As for the shared MCs, there is one set of memory request and response queues per MC in the shared memory structure, as shown in Figure~\ref{subfig:simulator_mc}.
At GPGPU-Sim side, on each memory tick received for a particular MC, it pushes a pending request, from its internal queue into the request queue in the shared memory structure in FIFO order.
Similarly, it pops pending read responses, if there are any, in FIFO order from the response queue in shared memory structure and pushes them into its internal response queue to be returned to corresponding SMs.
At gem5 side, once a pending memory tick is reset by GPGPU-Sim, the gem5 resumes to execute its portion of memory tick.
At front-end, an arbiter is used to select a request between the CPU and the GPU to push into front-end request queue for scheduling.
If the GPU wins the arbitration, it pops a GPU request present in the shared memory structure.
By default, FR-FCFS policy is applied on the front-end queue to schedule a request and push into the back-end command queue.
At back-end, it scans the command queue and queries the DRAM banks to issue appropriate commands.
When a read/write command is issued, the request is pushed into the response queue, with the ready time set according to CAS latency.
Any response that is intended for GPU will be popped from gem5's response queue when it is ready, and pushed into the response queue in the shared memory structure.

\section{Benchmarks}
\label{sec:bench}
We use 14 GPGPU benchmarks from Rodinia suite \cite{rodinia}. It covers applications from a wide range of domains, including pattern recognition, graph algorithms, fluid dynamics, medical imaging, physics simulation, data mining, multi-body dynamics, linear algebra, bioinformatics, grid traversal, and image processing.
Table~\ref{tab:gpubench} summarizes the name, abbreviation, memory intensity, row-buffer locality, and ratio of effective bandwidth to peak bandwidth.
\begin{table}[!b]
    \caption{GPU benchmarks}
    \centering
    \begin{tabular}{|l||l|c|c|c|}
        \hline
        \textbf{Benchmark} & \textbf{Abbrev.} & \textbf{MPKC} & \textbf{RBL} & \textbf{Eff/Peak BW} \\
        \hhline{|=||=|=|=|=|}
        Needleman-Wunsch & NW & 114 & 0.21 & 23\% \\
        \hline
        Streamcluster & SC & 152 & 0.69 & 30\% \\
        \hline
        HotSpot & HSP & 297 & 0.81 & 59\% \\
        \hline
        Particle Filter & PTFT & 313 & 0.98 & 63\% \\
        \hline
        LU Decomposition & LUD & 334 & 0.93 & 67\% \\
        \hline
        Back Propagation & BKP & 354 & 0.94 & 71\% \\
        \hline
        SRAD & SRAD & 397 & 0.55 & 79\% \\
        \hline
        Breadth-First Search & BFS & 402 & 0.54 & 80\% \\
        \hline
        LavaMD & LavaMD & 433 & 0.96 & 87\% \\
        \hline
        PathFinder & PFD & 436 & 0.97 & 87\% \\
        \hline
        CFD Solver & CFD & 448 & 0.94 & 90\% \\
        \hline
        K-Means & KMN & 464 & 0.90 & 93\% \\
        \hline
        Heart Wall & HTW & 468 & 0.92 & 94\% \\
        \hline
    \end{tabular}
    \label{tab:gpubench}
\end{table}
Memory intensity is measured by number of memory requests per kilo memory cycles (MPKC).
Row-buffer locality (RBL) is measured by the ratio of row-buffer hits to total number of accesses.
We tag the parallel portion of each GPU benchmark, which generally dominates the total execution time of these data-parallel applications, as region of interest (ROI).
Hence we perform the analysis and evaluation only on the parallel region.
Due to the lack of graphics application support by simulators available in academia, we did not include graphics applications in our evaluation.
Yet according to Table~\ref{tab:gpubench}, the evaluated benchmarks cover a wide range of memory intensities and row-buffer localities.
Therefore, we expect that our evaluation results can be also applied to graphics application domain.

On the CPU side, we use 21 SPEC CPU2006 benchmarks \cite{spec2006}.
The name and memory intensity measured by memory requests per kilo instructions (MPKI) are summarized in Table~\ref{tab:cpubench}.
\begin{table}[!b]
    \caption{CPU benchmarks}
    \centering
    \begin{tabular}{|l|c||l|c||l|c|}
        \hline
        \multicolumn{2}{|c||}{\textbf{Low}} & \multicolumn{2}{c||}{\textbf{Medium}} & \multicolumn{2}{c|}{\textbf{High}} \\
        \hline
        \textbf{Benchmark} & \textbf{MPKI} & \textbf{Benchmark} & \textbf{MPKI} & \textbf{Benchmark} & \textbf{MPKI} \\
        \hhline{|=|=||=|=||=|=|}
        povray & 0.005 & astar & 0.730 & leslie3d & 11.2 \\
        \hline
        GemsFDTD & 0.006 & gobmk & 0.841 & libquantum & 14.9 \\
        \hline
        tonto & 0.034 & calculix & 1.17 & bwaves & 15.3 \\
        \hline
        h264ref & 0.106 & hmmer & 1.21 & milc & 21.8 \\
        \hline
        sjeng & 0.461 & sphinx3 & 1.29 & lbm & 55.7 \\
        \hline
        gromacs & 0.599 & namd & 1.33 & mcf & 73.4 \\
        \hline
        - & - & bzip2 & 1.89 & mcf & 73.4 \\
        \hline
        - & - & omnetpp & 4.04 & - & - \\
        \hline
        - & - & soplex & 5.95 & - & - \\
        \hline
    \end{tabular}
    \label{tab:cpubench}
\end{table}
All CPU benchmarks in Table~\ref{tab:cpubench} are categorized in three groups (high, medium, low) based on their memory intensities.
We fast-forward the first one billion instructions in functional mode and then simulate one billion instructions in cycle-accurate mode.

\section{Power Modeling}
\label{sec:pwr_model}
In this thesis, we need to model power consumptions at different voltage/frequency levels for the CPU and the GPU, given the processor configuration and hence the power consumption at nominal voltage/frequency.
More specifically, we are interested in the peak power consumption that are related to chip power budget, instead of the average power consumption or energy consumption.
Therefore, we avoid the complexity of involving runtime statistics, such as the cache assess rate and arithmetic unit utilization, into the power model.
The key problem is how the power consumption scales with voltage/frequency.

In our method, first we apply the processor configuration parameters to McPAT \cite{mcpat} and GPUWattch \cite{gpuwattch} for the CPU and the GPU, respectively.
This allows us to calculate the ratio between dynamic and leakage power of the CPU and the GPU at nominal supply voltage (V\textsubscript{dd}).
Following the modeling methodology presented in \cite{lee2011dvfcs}, we obtain the frequency-voltage scaling factors by measuring the delay of a 24-stage fanout of 4 inverter chain at different V\textsubscript{dd} levels, and we obtain the leakage-voltage scaling factor by measuring the leakage current of a dummy circuit including a large number of INV (50\%), NAND (30\%), and NOR (20\%) gates.
The circuit simulation is done by HSPICE with the transistor model of 32nm technology \cite{ptm45nm, ptmurl}.
With (i) the ratio between dynamic and leakage power consumption of the CPU and the GPU at the nominal V\textsubscript{dd}, (ii) the frequency and leakage scaling factors as functions of V\textsubscript{dd}, and (iii) the power consumptions of the CPU and the GPU at nominal V\textsubscript{dd}, we can calculate the power consumptions of the CPU and the GPU as a function of V\textsubscript{dd}.

