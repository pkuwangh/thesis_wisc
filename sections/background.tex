\chapter{Background and Related Works}
\label{ch:background}
This chapter describes background material and previous work that are related to this thesis in general.
Other closely related works are discussed in each following chapter.
Section \ref{sec:heter_proc} introduces heterogeneous processor architecture and summarizes techniques to partition and run a parallel workload on both the CPU and the GPU cooperatively.
Section \ref{sec:pwr_manage} introduces power management techniques and explores recent advances in power-performance optimization for multi-core and many-core processors.
Section \ref{sec:dram_arch} describes DRAM basics including array organization and interface, and describes advanced proposals to improve bandwidth.
Section \ref{sec:mem_sched} describes memory controllers and various scheduling techniques.
Section \ref{sec:pcm} introduces phase change memory as a candidate for future main memory.

\section{Heterogeneous Processors}
\label{sec:heter_proc}

\subsection{Architecture}
\label{subsec:heter_arch}
Many computing systems have both a CPU and a GPU. However, previous heterogeneous computing system typically comprises discrete CPU and GPU components, hence requiring data transfer between the CPU and the GPU through slow chip-to-chip interconnects (i.e., PCIe).
In order to avoid the performance bottleneck due to data transfers and improve the power efficiency by introducing heterogeneity on chip, new architectures such as AMD's Llano processor \cite{amdllano} and Intel's Sandy Bridge processor \cite{intelsandybridge} began to integrate both the CPU and the GPU in a single chip.
Furthermore, the physical integration of the CPU and the GPU in the same chip is only the first step towards a real heterogeneous computing system.
On-going and future steps include (i) the architectural integration with a unified address space and fully coherent memory between the CPU and the GPU, and (ii) the system integration with preemption and context switch enabled on GPU, according to AMD's heterogeneous system architecture (HSA) roadmap \cite{amdhsa}.

Figure~\ref{fig:heter_proc} shows a block diagram of a single-chip heterogeneous processor (SCHP), similar to processors from AMD and Intel.
\begin{figure}[!b]
    \centering
    \includegraphics[width=3.6in,clip=false,trim=0 0 0 0]{figures/heter_proc.pdf}
    \caption[Block diagram of an SCHP comprised of CPU and GPU.]{Block diagram of an SCHP comprised of a CPU and a GPU. The memory controllers (MCs) are shared by both the CPU and the GPU.}
    \label{fig:heter_proc}
\end{figure}
For example, AMD's Llano APU integrates four x86 CPU cores and eighty VLIW-5 Radeon GPU cores in a single chip in 32nm technology \cite{amdllano}.
Typically, a set of GPU cores is grouped together as a streaming multiprocessor (SM) (in NVIDIA's terminology, or compute unit in AMD's terminology).
Note that this thesis uses NVIDIA's terms more often because the GPU simulator we use models an architecture similar to NVIDIA's GPU.
An L1 cache is private to each SM and all SMs share an L2 cache.
The GPU L1 caches are usually write-through and non-coherent, while the GPU L2 cache is expected to be coherent with the CPU L2 cache(s), either private to one CPU core all shared by all CPU cores.
Intel's SCHPs typically share an L3 cache between the CPU and the GPU, while AMD's SCHPs do not have such a shared last-level cache (LLC).
From a certain perspective, AMD's design removes the L3 cache and packs a stronger GPU in its SCHPs.
In this thesis, we are more interested in SCHPs similar to AMD's processors for its more competitive GPU performance.

As shown in Figure~\ref{fig:heter_proc}, the CPU and the GPU are in separate power domains, supported by separate voltage regulators and power delivery networks.
Therefore, the voltage/frequency levels of the CPU and the GPU can be controlled independently, while the CPU and the GPU together must comply with the chip power budget.
Note that enabling fine-grain power domain, for example, per-core power domain, is a general trend that is already happening in the industry.
While its benefit is very obvious as it allows each small unit to run at its optimal voltage/frequency level, it was very expensive to implement as many large off-chip voltage regulators are required.
However, a breakthrough is found recently in Intel's Haswell processors \cite{intelhaswell}, featuring the fully integrated voltage regulator.
It uses two-stage voltage regulation.
At the first stage, a single off-chip voltage regulator generates the power supply to the processor chip, and the second stage consists of multiple parallel on-chip integrated voltage regulators, one for each major architectural component (i.e., one CPU core).
With more independent power domains enabled by integrated voltage regulators, the coordinated management of all different domains under a shared chip power budget will be more challenging and interesting.

On the other side, the CPU and the GPU also share the memory channels, accessing the same set of memory devices, as shown in Figure~\ref{fig:heter_proc}.
This sharing has several implications.
At software level, instead of having a separate address space for the GPU in traditional GPU devices, a lot of effort is made to have a unified address space between the CPU and the GPU \cite{amdhsa2}.
The GPU will also support virtual address translation with shared page tables and hence GPU can accesses pageable system memory freely via CPU pointers.
At hardware level, the limited off-chip memory bandwidth is shared between the CPU and the GPU.
Hence the memory controllers should be designed to reduce the interference, guarantee quality of service, and improve system performance. Previous studies related to memory scheduling for SCHPs are discussed in Section~\ref{sec:mem_sched} in a systematic way.

\subsection{GPU Computing and Workload Partitioning}
\label{subsec:work_part}
Early GPUs simply had fixed-function pipelines.
Over the years, even though more programmability is added to GPU incrementally, mapping general-purpose computation on GPU was still very complicated, requiring programming with graphics APIs \cite{gpucomputing}.
In recent years, GPUs have become more programmable for general-purpose computing \cite{gpucomputing, gpuera}.
Programming frameworks, such as NVIDIA's compute unified device architecture (CUDA) \cite{cuda} and the open computing language (OpenCL) \cite{opencl} leverage standard C/C++ language with only a few extensions, such as built-in thread index variables.
These scalable programming models with C-like syntax allow programmers to launch a massive number of threads executing a kernel function on different data, without requirements of deep knowledge in graphics programming or GPU architecture.
Within the past few years, a large number of parallel applications have been ported to GPUs for their higher power efficiency than CPUs, such as computational fluid dynamics, electric design automation, machine learning, etc.

As both the CPU and the GPU have the capability of general-purpose computation, it is possible to partition a large, parallel workload and distribute the computation to both the CPU and the GPU in a computing system.
Figure~\ref{fig:workload_part_example} shows a simple example of partitioning a vector addition application.
\begin{figure}[!t]
    \centering
    \includegraphics[width=2.0in,clip=false,trim=0 0 0 0]{figures/workload_part_example.pdf}
    \caption[Example of partitioning a vector addition]{Example of partitioning a vector addition on both the CPU and the GPU.}
    \label{fig:workload_part_example}
\end{figure}
The performance benefit is obvious as the overall throughput is definitely higher than the throughput of either the CPU or the GPU alone.
Note that such workload partitioning does not necessarily require the CPU and the GPU in the same chip, although it can be more appealing on SCHPs since the CPU and the GPU are more tightly integrated, as discussed in next subsection.

In fact, there have been early studies on the programming frameworks for heterogeneous systems with discrete GPUs.
EXOCHI \cite{exochi2007} abstracts heterogeneous accelerators as ISA-based computing resources that are tightly coupled with the CPU and provides a programming environment that supports accelerator-specific inline assembly.
Its runtime can spread parallel computations across the heterogeneous cores to optimize performance.
Merge \cite{merge2008} is a library-based framework to program heterogeneous processors using the map-reduce paradigm.
However, the computation-to-core mappings in Merge are determined by the programmer.
Later, C. Luk et al. proposed Qilin \cite{qilin2009}, a programming system that can adaptively map computations to processing elements.
Qilin relies on a regression model to predict kernel execution times on the CPU and the GPU, and optimally partitions the workload during dynamic compilation time.
Yet, Qilin is built on top of Intel's threading building block (TBB) for CPU and NVIDIA's CUDA for GPU, and it requires extensive offline profiling to train the performance model.

\subsection{OpenCL and Cooperative Heterogeneous Computing}
\label{subsec:opencl_chc}
The above subsection discusses GPU computing model and workload partitioning between the CPU and the GPU in general, and describes early work on workload partitioning on heterogeneous systems with discrete GPUs.
In this subsection, we focus more on SCHPs, describing OpenCL standard in more detail and recent work on partitioning a single workload on an SCHP, which is referred to as cooperative heterogeneous computing.
While CUDA is more specific to NVIDIA's GPU, OpenCL is designed to support parallel execution on various heterogeneous computing devices (e.g., CPUs, GPUs, and accelerators).
OpenCL allows any processors in the computing system to act as peer computational units by abstracting the specifics of the underlying hardware.
The software stack of OpenCL is composed of: (i) a platform layer that queries and selects compute devices in the platform, initializes the compute device(s), and creates compute contexts and command queue(s); (ii) a runtime layer that manages computational resources and executes compute kernels; and (iii) a compiler that supports a subset of standard C/C++ with appropriate language extensions and compiles executable kernels online.

The introduction of OpenCL standard makes an important step towards efficient cooperative heterogeneous computing to allow the various compute devices to have an identical look from high-level software perspective.
With current OpenCL standard, a single parallel application can utilize both the CPU and the GPU within the same execution context.
Figure~\ref{fig:opencl} shows how an OpenCL application can be partitioned and distributed to the CPU and the GPU on an SCHP.
\begin{figure}[!t]
    \centering
    \includegraphics[width=4.0in,clip=false,trim=0 0 0 0]{figures/opencl.pdf}
    \caption[An OpenCL application in a heterogeneous system.]{An OpenCL application in a heterogeneous system. Workload partitioning happens at the granularity of work group.}
    \label{fig:opencl}
\end{figure}
The kernels and memory objects created at the context level are shared between the CPU and the GPU by default.
The workload partitioning can happen at the granularity of work group, since there is no explicit synchronization mechanism between two work groups.
A subset of all work groups can be submitted to the command queue of the CPU and remaining work groups to the GPU command queue.

The main challenges towards true cooperative heterogeneous computing with current OpenCL standard and platforms include (i) memory space management, and (ii) load balancing \cite{intelopencl}.
However, we have seen both existing workaround solutions and industrial or academic efforts for both problems.
Currently, although the memory objects are shared in existing heterogeneous computing systems, the CPU and the GPU actually have separate copies of data in separate memory space \cite{bittechhuma}.
A workaround solution is to use properly-aligned, non-overlapping sub-buffers such that the CPU and the GPU can simultaneously write to the same memory buffer \cite{intelsubbuffer}.
Furthermore, a fundamental solution is also coming with the industrial trend towards tighter integration of the CPU and the GPU sharing the memory space, such as recently announced AMD's heterogeneous unified memory access (hUMA) for next-generation APUs \cite{bittechhuma}.
As for the load balancing issue, ideally there should have a shared command queue to dynamically schedule the computation and balance the workload between the CPU and the GPU, instead of having separate command queues as shown in Figure~\ref{fig:opencl}.
Early studies on load balancing have been already discussed in Section~\ref{subsec:work_part}.
Recently, based on OpenCL, D. Grewe and M. O'Boyle proposed a compile-time approach for partitioning and mapping OpenCL programs on SCHPs \cite{cgocpugpuopencl, compilercpugpuopencl}.
It relies on the compiler analysis of kernel code features, and statically predicts the optimal partitioning using machine-learning techniques.
Intel also demonstrated an OpenCL sample for N-Body simulation using an Intel Sandy Bridge processor \cite{intelnbody}.
It partitions the bodies between the CPU and the GPU, and the partitioning can be enforced by setting the corresponding global work size, when enqueuing a kernel to the CPU or GPU command queue.
And it iteratively balances the load according to the runtime statistics from previous iteration.

\section{Power Management}
\label{sec:pwr_manage}

\subsection{Power Reduction Techniques}
\label{subsec:pwr_down}
Most modern processors employ dynamic voltage/frequency scaling (DVFS) to improve its power efficiency \cite{dvfs2006}.
Depending on the performance requirement, the supply voltage and operating frequency can be scaled down to reduce power consumption of digital circuits.
This can be controlled by either a hardware power management unit or the operating system.
To further reduce leakage power consumption, power-gating techniques uses high-threshold-voltage sleep transistors to cut off a circuit block from power supply when it is idle.
In a multi-core processor, applying per-core power-gating dynamically to an idle core can vary the number of operating cores based on the parallelism of the running application \cite{lijian2006}.
This is known as dynamic core scaling (DCS).
DVFS and DCS can be combined (i.e., dynamic voltage/frequency/core scaling (DVFCS)) to optimize power-performance in multi-threaded applications.

\subsection{Dynamic Power-Performance Optimization}
\label{subsec:pwr_perf}
There have been many studies on optimizing performance and power consumption of processors from various perspectives.
In this subsection, I summarize previous studies related to dynamic power-performance optimization on multi-core or many-core processors using DVFCS.
J. Li and J. Martinez proposed a runtime DVFCS algorithm that minimizes the power consumption of homogeneous chip multi-processors (CMPs) under a performance constraint \cite{lijian2006}.
The power-performance characteristics of applications are analyzed to effectively prune the optimization search space and it is demonstrated that the proposed runtime DVFCS algorithm produces near-optimal solutions.
M. Curtis-Maury et al. proposed to use a multi-dimensional, online performance predictor to optimize the power and performance using DVFCS \cite{dvfcsprediction}. J. Lee et al. proposed a runtime algorithm that scales (i) the number of active SMs and (ii) the voltage/frequency of both SMs and on-chip interconnects to maximize the throughput of a GPU under a power constraint \cite{lee2011dvfcs}.

\section{DRAM Architecture}
\label{sec:dram_arch}

\subsection{DRAM Organization and Operation}
\label{subsec:dram_basic}
The modern memory system typically has multiple channels operating independently.
A channel connects a memory controller with one or more dual inline memory modules (DIMMs), each of which is comprised of one or more ranks.
A rank consists of multiple banks (i.e., 8 for DDR3), each of which can operate independently and service an outstanding memory request.
In order to access a block of data (e.g., 64 bytes), the entire row (e.g., 8KB) of a bank is read into the row buffer, which is referred to as an activate.
Subsequent access to the same row can be serviced directly by the row buffer (i.e., row-buffer hit), resulting in lower latency.
In contrast, an access to a different row results in row-buffer conflict, requiring additional cycles to precharge the bitlines and activate the new row.

The concept of rank/bank described in the above paragraph is actually from a logical perspective.
As for the physical view, a rank consists of multiple DRAM chips, which operate in lockstep, to provide necessary bandwidth and capacity.
For example, a 4GB DIMM without ECC may have eight 4Gbit x8 devices in a single rank, while each device provides 8 bits to support the 64-bit data path.
A contemporary DRAM device, which contains billions of DRAM cells, leverages a hierarchical structure to balance latency, bandwidth, and energy consumption.
A DRAM chip consists of multiple banks.
The banks from all DRAM devices in a rank with the same bank index form a logical bank as stated in the above paragraph.
Each physical bank is constituted with thousands of DRAM mats arranged in a two-dimensional fashion.
A mat, which is the building block in a DRAM device, typically consists of 512x512 DRAM cells and shares a set of peripheral circuits such as a row decoder, sense amplifiers, global I/O lines, etc.

\subsection{DDR Interface}
\label{subsec:ddr}
The current DDRx DRAM technology evolves from synchronous DRAM (SDRAM) to DDR, DDR2, and DDR3, with DDR4 coming out.
In this subsection, we take DDR3-1600 as an example to explain the DDRx interface with specific numbers.
The I/O frequency of address and command bus is 800MHz, while the bit rate of data bus is 1600Mbps per link (i.e., double data rate, $2\times$800Mbps).
With the typical channel width of 64 bits (i.e., 8 x8 devices), the per-channel bandwidth is 12.8GB/s.
Therefore, the transmission time for a data block, which is typically 64Bytes, is 5ns.

The internal DRAM array operates at a much lower speed (i.e., 200MHz) than I/O bus.
Hence, a DRAM array has 8 times more global data lines than the data I/O width, such that more bits are prefetched in each cycle to match the I/O bandwidth.
Specifically, each DRAM chip outputs 64bits every 5ns (i.e., 1 internal cycle) and they are consumed by the data I/O bus over 8 bursts in 5ns (i.e., 4 I/O clock cycles).

\subsection{Advanced Techniques to Improve Bandwidth}
\label{subsec:bwboost}
With the increasing demand for high bandwidth, large capacity, and low power consumption of memory systems, various novel DRAM architecture have been proposed.
Fully-buffered DIMM (FBDIMM) technology \cite{fbdimmhpca} uses a narrow but fast bus between the memory controller and an FBDIMM.
The conventional multi-drop bus is replaced with a point-to-point link between the memory controller and an intermediate buffer (i.e., advanced memory buffer (AMB)) on an FBDIMM.
The key motivation of FBDIMM is to increase the total capacity since multiple FBDIMMs can connect in series in a channel while the signal integrity degradation on high-speed multi-drop bus limits the number of normal DIMMs per channel.
A study on buffer-on-board (BOB) memory system \cite{bob} generalizes the technique of placing the intermediate buffer and logic between the processor and DRAM, and using a narrow but fast bus between the processor and the intermediate buffer.
In order to further boost the bus speed with existing DRAM devices, decoupled DIMM \cite{decoupleddimm} decouples the bandwidth match between a memory bus and a single rank of DRAM devices.
It allows the memory bus to operate at a much higher data rate than that of a rank, while the combined bandwidth of multiple ranks (e.g., 2) still match the bandwidth of memory bus.
A similar idea is exploited by buffer output on module (BOOM) \cite{boom} to enable the use of mobile memory devices for high-end servers for low power consumption.
It aggregates multiple ranks of low-frequency, low-power DRAM devices to provide high bandwidth.

Recent advance in semiconductor manufacturing enables more aggressive proposals to improve bandwidth.
3D-stacking technology \cite{3dstacking, 3dmemarch} can provide significantly higher bandwidth by stacking DRAM dies atop a processor die and increasing the bus width with a large number of through silicon vias (TSVs).
Yet its capacity cannot scale with the increasing capacity demand due to thermal and mechanical reliability issues.
Therefore, 3D-stacked DRAM has been proposed and actively studies as a last-level cache \cite{lohhilldramcache, alloycache, footprintcache}.
The silicon interposer technology \cite{siliconinterposer} (also known as 2.5D-stacking) allows integration of multiple stacks of DRAM dies around a processor die on the same package substrate, alleviating the capacity issue of 3D-stacking technology.
However, such complicated integration technology may lead to even higher manufacturing cost and lower yield than 3D-stacking \cite{interposercost}.
Note that 3D-stacking technology already suffers from high cost and low yield due to additional processing steps such as die thinning and newly introduced defects such as die warpage, TSV failure and misalignment.

\section{Memory Scheduling}
\label{sec:mem_sched}
On the processor side, a memory controller (MC) interacts with the DIMMs of each channel.
An MC enqueues memory requests in its request buffer, keeps track of the internal state of each bank, and issues DRAM commands complying with various timing constraints.
Advanced MCs can check all requests in the request buffer and schedule the requests to improve channel utilization and system performance.
There have been many studies on memory scheduling algorithms and techniques.
In this section, I describe previous work on memory scheduling for uniprocessors, CMPs, GPUs, and SCHPs.

S. Rixner et al. introduced the well-known first-ready first-come-first-service (FR-FCFS) scheduling policy that prioritizes row-buffer hits to improve memory system utilization \cite{frfcfs}.
FR-FCFS has been widely accepted for uniprocessor, while it does not provide any fairness guarantees when used in CMPs since it emphasizes the throughput of DRAM side.

Recently proposed scheduling algorithms attempt to make memory controller aware of the different characteristics of applications running on an CMP.
Fair queuing memory system (FQMS) \cite{fqms} attempts to guarantee each thread receives its allocated share of memory bandwidth, and hence provides quality-of-service (QoS) to all threads.
Each incoming request is annotated with a virtual start and finish time.
Virtual times represent the times on a virtual private memory system whose performance is scaled with the allocated bandwidth share.
Then the scheduler prioritizes the request with earliest virtual finish time since it represents the deadline to offer QoS to that thread.
Parallelism-aware batch scheduler (PAR-BS) \cite{parbs} issues requests in batches to provide fairness and maintains original bank-level parallelism of memory requests within a single application.
Adaptive per-thread least-attained-service scheduler (ATLAS) \cite{atlas} prioritizes requests from the application that has received least memory service.
Thread cluster memory scheduling (TCM) \cite{tcm} categorises applications into two clusters, latency-sensitive and bandwidth-sensitive clusters, based on bank-level parallelism, row-buffer locality and memory intensity of applications.
TCM improves both system throughput and fairness by prioritizing requests from the latency-sensitive cluster over the bandwidth-sensitive cluster.

GPU applications exhibit very different memory access characteristics.
G. Yuan et al. proposed to employ an interconnection network arbitration scheme to preserve the inherent row-buffer locality of individual memory request stream from a single GPU core \cite{inordergpusched}.
As a result, the performance of a GPU with a simple in-order scheduler can get close to a GPU with an aggressive out-of-order scheduler.
N. Chatterjee et al. made another interesting observation that the high variance in the latency of requests from different threads of a single warp can degrade the GPU performance running irregular GPU applications \cite{latencydivergence}.
To solve this latency divergence issue, the paper proposed to avoid inter-warp interference in an MC and coordinate scheduling decisions across multiple MCs.

As for SCHPs, a QoS-aware memory controller was proposed to monitor the progress of rendering process of graphics applications and dynamically adjust the priority of CPU and GPU memory requests accordingly \cite{qosdac2012}.
It ensures that the GPU meets the real-time constraint for graphics applications with minimum negative impact on CPU performance.
Staged memory scheduler \cite{sms} decouples the primary functions of a memory controller into three stages such that the request buffer can be larger to obtain enough visibility. It improves both fairness and system performance by maximizing CPU performance while satisfying certain target GPU performance (i.e., frame rate).

\section{Phase Change Memory}
\label{sec:pcm}
With aggressive downscaling of feature size, the scalability of charge-based memory technologies such as DRAM is limited due to less reliable charge storage and data sensing.
Therefore, various resistance-based memory technologies such as phase change memory (PCM) \cite{pcmwong2010, pcmlee2009, pcmqureshi2009, pcmlecture} and magnetoresistive random-access memory (MRAM) \cite{sttram} have been proposed as promising alternatives to DRAM.

PCM employs a chalcogenide alloy such as Ge2Sb2Te5 (GST), which can represent two unique structural phases based on the degree of heating temperature: (i) "programmed" amorphous phase and (ii) "erased" crystalline phase \cite{pcmibm}.
Once a heating element of PCM applies high temperature, the target GST's chalcogenide crystallinity is disappeared.
This reset operation makes GST amorphous phase, which can indicate an erased state with a high resistance value.
On the other hand, heating GST to a certain temperature transforms the target GST from the amorphous phase to the crystalline phase.
Through this set operation, GST can represent a written state with a low resistance value.
In practice, PCM systems apply varying pulse signals that flow through the heating element to control the temperature \cite{pcmwong2010}, as shown in Figure~\ref{fig:pcm_basic}.
\begin{figure}[!b]
    \centering
    \includegraphics[width=5.0in,clip=false,trim=0 0 0 0]{figures/pcm_basic.pdf}
    \caption{Phase change memory: basic structure and operations.}
    \label{fig:pcm_basic}
\end{figure}
While the set and reset operations require high and long pulse signal, a read operation only needs low voltage to sense the device resistance.

Unlike typical charge-based memories, PCM can store multiple bits into a single memory cell.
A multi-level cell (MLC) based PCM employs more resistance levels between the amorphous and crystalline phases.
As a consequence, each state of a target GST representing multiple bits will have a smaller programmed resistance region than single-level cell (SLC) based PCM \cite{pcmmlchpca2012}, as depicted in Figure~\ref{fig:pcm_mlc}.
\begin{figure}[!b]
    \centering
    \includegraphics[width=4.5in,clip=false,trim=0 0 0 0]{figures/pcm_mlc.pdf}
    \caption{SLC- and MLC-based PCMs.}
    \label{fig:pcm_mlc}
\end{figure}
While it is desired to have an accurate programming method to accommodate the smaller programmed resistance region, a single programming pulse signal used by SLC-based PCM is not a viable solution to program multiple resistance levels of MLC-based PCM.
This is because variations in GST due to process and material variability may lead to a wider resistance distribution on a single pulse signal than the distribution of each MLC-based PCM's resistance region.
To address this issue, an iterative multi-level programming technique is typically employed \cite{pcmwritehpca2010}.
Specifically, with the iterative technique, MLC-based PCM (i) applies a short amplitude of the programming pulse, (ii) updates the amplitude based on the error observed between the programmed state and target crystalline resistance region, and (iii) programs again.
This process of program-and-verification is performed until the resulting state matches with the target resistance level.

Due to the dynamics of intrinsic trap on chalcogenide, the resistivity of the amorphous phase tends to keep increasing \cite{pcmretention}.
This process drifts the resistance distribution on a programmed state over time, which can in turn lead to data corruption.
In contrast to SLC-based PCM, MLC-based PCM has much smaller resistive margins that are not enough to tolerate the resistance drift over time, as shown in Figure~\ref{fig:pcm_mlc}.
To avoid errors on data corruption, MLC-based PCM requires appropriately refreshing its memory cells based on the retention time.
For example, 2-bit and 3-bit MLC should perform a refresh for every two months and five minutes, respectively.

