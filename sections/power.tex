\chapter{Workload and Power Budget Partitioning for Heterogeneous Processors}
\label{ch:power}

Portions of this chapter were previously published as \cite{wang2012pact} in ACM 21st international conference on parallel architectures and complication techniques in 2012.

\section{Introduction}
\label{sec:power_intro}
As described in Section~\ref{subsec:work_part}, recent studies demonstrate that workload partitioning between the CPU and the GPU can improve the overall throughput and/or power efficiency of a heterogeneous computing system comprised of discrete CPU and GPU components \cite{exochi2007, merge2008, qilin2009}.
Such workload partitioning can also improve the overall throughput of single-chip heterogeneous processors (SCHPs).
However, the CPU and the GPU must share a chip-level power budget due to their integration in a single chip, and the CPU or GPU must also satisfy its own power constraint imposed by thermal and reliability constraints.
Typically, the performance of a CPU or GPU is positively correlated to its power consumption, yet the CPU and the GPU exhibit different performance and power efficiencies depending on the characteristics of the executed workload.
For efficient power management, SCHPs typically support separate voltage/frequency (V/F) domains for the CPU and GPU cores.
They also provide power-gating (PG) devices for each CPU and GPU core (or for each group of GPU cores).
These two features enable dynamic voltage/frequency scaling (DVFS) and dynamic core scaling (DCS), allowing an SCHP to dynamically allocate its total power budget between the CPU and the GPU under the chip power constraint.
Therefore, joint optimization of both workload and power budget partitioning between the CPU and the GPU can help to improve the overall throughput of SCHPs.

The remainder of this chapter is organized as follows. Section~\ref{sec:power_config} details the system configuration. Section~\ref{sec:power_jointopt} demonstrates the potential throughput improvements achieved by joint workload and power budget partitioning with an oracle approach. Section~\ref{sec:power_runtime} describes our proposed runtime algorithm to determine optimal power budget partitioning and evaluates its effectiveness. Section~\ref{sec:power_summary} concludes this chapter.

\section{System Configuration}
\label{sec:power_config}
In this study, we configure our simulator presented in Section~\ref{sec:perf_simulator} to model an SCHP with 4 CPU cores and 12 GPU SMs.
We set the GPU configuration based on a discrete GPU, NVIDIA's GTS 260M, which has a similar peak throughput as the GPU integrated in AMD's Llano SCHPs \cite{specamdllano}.
The GTS 260M has 12 SMs with 396 GFLOPs of throughput at the nominal operating frequency \cite{specnvidiagts260m}.
Table~\ref{tab:power_config} summaries the key configuration parameters.
\begin{table}[!t]
    \caption[SCHP configuration.]{Key configuration parameters for the evaluated single-chip heterogeneous processor.}
    \centering
    \begin{tabular}{|c|c||c|c|}
        \hline
        \# of CPU cores & 4 & \# of GPU SMs & 12 \\
        \hline
        \multirow{2}{*}{CPU freq/volt} & 1.67-3.44GHz & \multirow{2}{*}{GPU freq/volt} & 350-710MHz \\
                                       & 0.72-0.99V & & 0.72-0.99V \\
        \hline
        CPU core width & 4 & GPU SIMD width & 16384 \\
        \hline
        \multirow{2}{*}{CPU IL1 \& DL1} & 64KB/2-way/64B & GPU \# of threads & \multirow{2}{*}{1024} \\
                                        & 2-cycle & per SM & \\
        \hline
        \multirow{2}{*}{CPU L2 per core} & 1MB/16-way/64B & GPU \# of CTAs & \multirow{2}{*}{8} \\
                                         & 20-cycle & per SM & \\
        \hline
        CPU mis-pred & \multirow{2}{*}{14 cycles} & GPU \# of registers & \multirow{2}{*}{16384} \\
             penalty & & per SM & \\
        \hline
        CPU store buffer & 16 per core & GPU L1 per SM & 32KB \\
        \hline
        CPU core BTB & 8K/4-way & GPU warp size & 32 \\
        \hline
        Memory freq & DDR3-1866 & \# of MCs/scheduling & 2/FR-FCFS \\
        \hline
    \end{tabular}
    \label{tab:power_config}
\end{table}

The total chip power budget is assumed as 100W according to AMD's Llano A8-3850 SCHPs \cite{specamdllano}.
The nominal operating voltage is assumed to be 0.9V as suggested by the transistor model of 32nm technology \cite{ptmurl}.
The CPU and the GPU are assumed to operate at 2.9GHz and 600MHz at the nominal voltage, respectively, again based on the specifications of AMD's Llano A8-3850 \cite{specamdllano} SCHPs and NVIDIA's GeForce GTS 260M GPU \cite{specnvidiagts260m}.
With the power modeling methodology presented in Section~\ref{sec:pwr_model}, we can obtain the frequencies and power consumptions at different operating voltages, listed in Table~\ref{tab:power_vf}.
\begin{table}[!b]
    \caption[Voltage vs. frequency and power consumption.]{Voltage vs. frequency and power consumption of a CPU core and a GPU SM.}
    \centering
    \begin{tabular}{|c||c|c|c|c|}
        \hline
        \multirow{2}{*}{\textbf{Voltage (V)}} & \multicolumn{2}{c|}{\textbf{Freq (GHz)}} & \multicolumn{2}{c|}{\textbf{Power(Watts)}} \\ \cline{2-5}
                                              & \textbf{CPU} & \textbf{GPU} & \textbf{CPU} & \textbf{GPU} \\
        \hhline{|=||=|=|=|=|}
        0.72 & 1.67 & 0.35 & 7.1 & 1.2 \\
        \hline
        0.75 & 1.89 & 0.39 & 8.7 & 1.4 \\
        \hline
        0.78 & 2.09 & 0.43 & 10.4 & 1.7 \\
        \hline
        0.81 & 2.31 & 0.48 & 12.4 & 2.1 \\
        \hline
        0.84 & 2.51 & 0.52 & 14.6 & 2.4 \\
        \hline
        0.87 & 2.70 & 0.56 & 17.1 & 2.8 \\
        \hline
        0.90 & 2.90 & 0.60 & 20.0 & 3.3 \\
        \hline
        0.93 & 3.07 & 0.64 & 23.2 & 3.9 \\
        \hline
        0.96 & 3.26 & 0.68 & 27.0 & 4.5 \\
        \hline
        0.99 & 3.44 & 0.71 & 27.0 & 5.2 \\
        \hline
    \end{tabular}
    \label{tab:power_vf}
\end{table}

Considering such an SCHP under power constraints, we establish four initial evaluation configurations all at the nominal V/F for our study: (i) GPU-oriented, (ii) CPU-oriented, (iii) GPU-only, (iv) CPU-only.
Configurations (i) and (ii) allocate relatively more power budget to the GPU and CPU, respectively, while configurations (iii) and (iv) use only either the GPU or CPU in the SCHP.
In configuration (i), 3 CPU cores at 0.9V/2.9GHz and 12 SMs at 0.9V/600MHz are active and consume 60W and 40W, respectively.
In configuration (ii), 4 CPU cores at 0.9V/2.9GHz and 6 SMs at 0.9V/600MHz are active and consume 80W and 20W, respectively.
In configuration (iii), only 12 SMs are active and consume 40W at 0.9V/600MHz.
In configuration (iv), 4 CPU cores are active and consume 80W at 0.9V/2.9GHz.
The maximum power consumption allowed for the CPU or GPU is limited by the maximum voltage (i.e., 0.99V as can be seen in Table~\ref{tab:power_vf}) allowed under thermal and reliability constraints.

\section{Joint Optimization of Workload and Power Budget Partitioning}
\label{sec:power_jointopt}

\subsection{The Need to Use Both the CPU and the GPU}
\label{subsec:power_cpugpu}
In an SCHP, a proper partitioning of the workload between the CPU and the GPU can improve overall throughput.
There are two reasons for this.
First, the performance of the CPU (or GPU) varies based on its power consumption when DVFS is employed.
Second, the CPU (or GPU) alone typically cannot consume the total chip power budget due to its thermal and/or maximum voltage constraint.
Consequently, the CPU and the GPU should operate in parallel to consume the entire chip power budget and thus maximize overall throughput.
To adjust the power allocation between the CPU and the GPU within the chip power budget, we explore two options: (i) dynamic core scaling (DCS) enabled by per-core and per-SM PG devices and (ii) dynamic voltage/frequency scaling (DVFS) enabled by two separate V/F domains.

Under such conditions, instead of assigning all the work (i.e., threads) to the GPU, whose maximum throughput is limited by its power constraint (i.e., 63W at 0.99V in our configuration), we can assign some work to the CPU such that the remaining chip power budget (i.e., 37W out of 100W in our baseline) can be fully utilized to maximize the overall throughput.
For the DCS option, all 12 GPU SMs are active and consume 40W while only 3 CPU cores operating at the nominal V/F consume 60W.
Alternatively, 4 CPU cores can consume 60W at lower V/F using DVFS, leaving 40W for the GPU.

\subsection{Power Allocation through DCS}
\label{subsec:power_dcs}
Figure~\ref{fig:power_tp_fixedpower} plots the maximum throughput of different SCHP configurations for all the benchmarks we examined.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_tp_fixedpower.pdf}
    \caption[Throughput of different SCHP configurations with fixed power budgets.]{Throughput of different SCHP configurations with fixed power budgets. Throughput is normalized to GPU-only.}
    \label{fig:power_tp_fixedpower}
\end{figure}
For CPU- and GPU-oriented configurations, we simulated 16 workload partitioning points (i.e., from 6.25\% to 100\% of the work assigned to the GPU) for each benchmark, and pick the partitioning point that maximizes throughput.
The optimal percentage values for all the benchmarks are tabulated in Table~\ref{tab:power_work_part}.
\begin{table}[!t]
    \centering
    \caption[Percentage of work assigned to GPU to optimize the throughput.]{Percentage of work assigned to GPU to optimize the throughput of each configuration.}
    \begin{tabular}{|c||c|c|}
        \hline
        \textbf{Benchmark} & \textbf{CPU-oriented} & \textbf{GPU-oriented} \\
        \hhline{|=||=|=|}
        BKP & 56.3\% & 81.3\% \\
        \hline
        BFS & 43.8\% & 68.8\% \\
        \hline
        CFD & 37.5\% & 62.5\% \\
        \hline
        HTW & 62.5\% & 81.3\% \\
        \hline
        HSP & 43.8\% & 68.8\% \\
        \hline
        LKT & 81.3\% & 93.8\% \\
        \hline
        LUD & 87.5\% & 93.8\% \\
        \hline
         NW & 87.5\% & 93.8\% \\
        \hline
        KMN & 68.8\% & 81.3\% \\
        \hline
       SRAD & 43.8\% & 68.8\% \\
        \hline
    \end{tabular}
    \label{tab:power_work_part}
\end{table}
The throughputs in Figure~\ref{fig:power_tp_fixedpower} are normalized to that of the GPU-only configuration in which all the CPU cores are disabled and all GPU SMs are active.
On average, the GPU-oriented configuration, which uses the full power budget of the SCHP, exhibits 27\% higher throughput than the GPU-only configuration.
In comparison, the CPU-oriented configuration, which also uses the full power budget of the SCHP, has 7\% lower throughput than the GPU-only configuration.
The CPU- and GPU-oriented configurations show 177\% and 278\% higher throughput than the CPU-only configuration, respectively.
This supports our earlier argument that the workload should be partitioned between the CPU and the GPU to maximize the throughput of an SCHP.

\subsection{Power Budget Partitioning Using DVFCS}
\label{subsec:power_dvfcs}
Figure~\ref{subfig:power_hotspot} and \ref{subfig:power_heartwall} plot the relative execution time versus the percentage of work assigned to the GPU for HotSpot and HeartWall, respectively.
These benchmarks are chosen because the optimal percentages of work assigned to the GPU of the remaining benchmarks are between those of these two benchmarks.
\begin{figure}[!t]
    \centering
    \subfloat[HotSpot]{
        \centering
        \includegraphics[width=3.6in,clip=false,trim=0 0 0 0]{figures/power_hotspot.pdf}
        \label{subfig:power_hotspot}
    }
    \qquad
    \subfloat[HeartWall]{
        \centering
        \includegraphics[width=3.6in,clip=false,trim=0 0 0 0]{figures/power_heartwall.pdf}
        \label{subfig:power_heartwall}
    }
    \caption{Execution time vs. percentage of work assigned to the GPU.}
    \label{fig:power_example}
\end{figure}
In addition to the CPU- and GPU-oriented configurations, we apply DVFS and DCS simultaneously (i.e., DVFCS).
This allows us to explore more fine-grain power allocations between the CPU and the GPU depending on the workload characteristics.

For DVFCS at each workload partitioning point, we initially perform an exhaustive search for all possible combinations of (i) V/F of the CPU and the GPU and (ii) the number of active CPU cores and GPU SMs, while satisfying the chip, CPU, and GPU power constraints.
In HotSpot, the highest throughput is achieved with 68.75\% of the work assigned to 12 active GPU SMs operating at 0.96V/680MHz and consuming 54.1W.
The rest of the work (31.25\%) is assigned to 4 active CPU cores operating at 0.78V/2.09GHz and consuming 41.5W.
In HeartWall, the highest throughput is achieved with 87.5\% of the work assigned to 12 active GPU SMs operating at 0.99V/710MHz and consuming 62.8W. The rest of the work (12.5\%) is assigned to 4 active CPU cores operating at 0.75V/1.89GHz and consuming 32.4W.

In both HotSpot and HeartWall, the minimum execution time (i.e., maximum throughput) is achieved when both the CPU and the GPU finish the assigned work approximately at the same time because the total execution time of an SCHP is the maximum of CPU and GPU execution times.
The optimal DVFCS configurations provide 13\% and 54\% higher throughput than the GPU- and CPU-oriented configurations, respectively, while using lower V/F for the CPU and allocating more power to the GPU to make it run faster.
Depending on how efficiently the CPU and the GPU can each process a given workload, the optimal configuration assigns relatively more work (and more power) to either the CPU or GPU.
This clearly demonstrates that proper workload and power budget partitioning based on workload characteristics is a key to maximize the overall throughput of SCHPs.

Figure~\ref{fig:power_tp_oracledvfcs} shows the throughput improvement with optimal DVFCS for all the benchmarks we examined.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_tp_oracledvfcs.pdf}
    \caption[Throughput improvement with fixed power budgets and optimal DVFCS.]{Throughput improvement with fixed power budgets and optimal DVFCS. Throughput is normalized to GPU-oriented.}
    \label{fig:power_tp_oracledvfcs}
\end{figure}
The throughput is normalized to the GPU-oriented configuration.
Compared to the results of the optimal workload partitioning under the fixed power budget for the CPU and the GPU (i.e., the CPU- and GPU-oriented configurations), the joint optimization of workload and power budget partitioning delivers up to 19\% and on average 13\% higher throughput than the GPU-oriented configuration, and up to 94\% and on average 54\% higher throughput than the CPU-oriented configuration.
Table~\ref{tab:power_dvfcs} summarizes the optimal percentage of the work assigned to the GPU and the optimal processor configuration (i.e., the number of active CPU cores and GPU SMs, and the operating V/F and power consumption of the CPU and the GPU) for all the benchmarks.
\begin{table}[!t]
    \caption[Optimal workload partitioning and power budget allocation.]{Optimal (1) percentage of work assigned to the GPU; (2) number of CPU cores (GPU SMs), (3) voltage in Volts for the CPU (GPU), (4) frequency in GHz for the CPU (GPU), and (5) power consumption of the CPU (GPU) in Watts.}
    \centering
    \begin{tabular}{|c||c|c|c|c|c|}
        \hline
        \textbf{} & \textbf{(1)} & \textbf{(2)} & \textbf{(3)} & \textbf{(4)} & \textbf{(5)} \\
        \hhline{|=||=|=|=|=|=|}
        BKP & 81.3 & 4 (12) & 0.78 (0.96) & 2.09 (0.68) & 41 (54) \\
        \hline
        BFS & 68.8 & 4 (12) & 0.78 (0.96) & 2.09 (0.68) & 41 (54) \\
        \hline
        CFD & 68.8 & 4 (12) & 0.75 (0.99) & 1.89 (0.71) & 34 (63) \\
        \hline
        HTW & 87.5 & 3 (12) & 0.75 (0.99) & 1.89 (0.71) & 26 (63) \\
        \hline
        HSP & 68.8 & 4 (12) & 0.78 (0.96) & 2.09 (0.68) & 41 (54) \\
        \hline
        LKT & 93.8 & 3 (12) & 0.78 (0.99) & 2.09 (0.71) & 31 (63) \\
        \hline
        LUD & 93.8 & 2 (12) & 0.87 (0.99) & 2.70 (0.71) & 34 (63) \\
        \hline
         NW & 93.8 & 3 (12) & 0.78 (0.99) & 2.09 (0.71) & 31 (63) \\
        \hline
        KMN & 87.5 & 3 (12) & 0.78 (0.99) & 2.09 (0.71) & 31 (63) \\
        \hline
       SRAD & 75.0 & 3 (12) & 0.81 (0.99) & 2.31 (0.71) & 37 (63) \\
        \hline
    \end{tabular}
    \label{tab:power_dvfcs}
\end{table}
In columns (2) to (5) of Table~\ref{tab:power_dvfcs}, the numbers in parenthesis are the values for the GPU.

\subsection{Insight into Joint Optimization Using DVFCS}
\label{subsec:power_insight}
To gain insight into developing an effective runtime DVFCS algorithm that can determine the optimal processor configurations, we plot the power efficiency ratio of the CPU to the GPU in Figure~\ref{fig:power_eff} for different configurations used to generate Figure~\ref{fig:power_tp_oracledvfcs}.
We define power efficiency at the optimal workload partitioning point, where both the CPU and the GPU exhibit the same execution time for a given workload, as follows:
\begin{doublespace}
    \begin{equation}
        \mathrm{
            \frac{P_{{EFF\_CPU}}}{P_{{EFF\_GPU}}} =
            \frac{\displaystyle \frac{W_{{CPU}}} {~P_{{CPU}}~(~V_{{CPU}}, F_{{CPU}}, N_{{CPU}})~}} {\displaystyle \frac{W_{{GPU}}} {~P_{{GPU}}~(~V_{{GPU}}, F_{{GPU}}, N_{{GPU}})~}}
        }
        \label{eq:powereff}
    \end{equation}
\end{doublespace}
where P\textsubscript{EFF\_CPU} and P\textsubscript{EFF\_GPU} are the power efficiency of the CPU and the GPU;
W\textsubscript{CPU} and W\textsubscript{GPU} are the percentage of work assigned to the CPU and the GPU;
P\textsubscript{CPU} and P\textsubscript{GPU} are the power consumption of the CPU and the GPU as a function of the number of active CPU cores and GPU SMs (N\textsubscript{CPU} and N\textsubscript{GPU}) and their V/F values (V\textsubscript{CPU}/V\textsubscript{GPU} and V\textsubscript{CPU}/V\textsubscript{GPU}).
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_eff.pdf}
    \caption[Power Efficiency ratio of the CPU to the GPU.]{Power Efficiency ratio of the CPU to the GPU for different processor configurations.}
    \label{fig:power_eff}
\end{figure}

As we can observe from the results shown in Figure~\ref{fig:power_eff}, the power efficiency ratios are much lower than 1.0 for the CPU- and GPU-oriented configurations;
on average 0.14 for both configurations.
Such a low ratio indicates that the CPU is using its power much less efficiently than the GPU, and therefore more power should be shifted to the GPU such that the SCHP can use the limited power budget more efficiently to improve overall throughput.
As the CPU operates at lower V/F to allocate more power to the GPU, it becomes more power-efficient.
Note that power efficiency increases as the operating voltage decreases.
This is because power is reduced super-linearly while frequency (and the performance of compute-bound applications) is degraded close to linearly.

On the other hand, as the GPU shifts to a higher V/F operating point by exploiting the power transferred from the CPU, the power efficiency of the GPU decreases.
After the DVFCS optimization, we observe that the power efficiency ratios significantly increase close to 1.0.
We note that the throughput can be maximized by allocating more power from a less power-efficient device to a more power-efficient device.
This brings the power efficiency ratio close to 1.0; the average power efficiency ratio with the optimal DVFCS is increased from 0.14 to 0.34.
Theoretically, the optimal DVFCS technique should perfectly balance the power efficiency between the CPU and the GPU, but (i) the limited V/F levels due to minimum operating V/F constraint for the CPU and the maximum operating V/F constraint for the GPU, (ii) discrete V/F levels, and (iii) discrete workload partitioning points prevent the optimal DVFCS from reaching the perfectly balanced point (i.e., a power efficiency ratio of 1.0) in Figure~\ref{fig:power_eff}.

To validate this argument, we plot the power efficiency ratio of HotSpot in Figure~\ref{fig:power_more_vf}, as we allow more V/F levels by providing both lower minimum and higher maximum operating V/F levels for the CPU and the GPU.
\begin{figure}[!b]
    \centering
    \includegraphics[width=4.0in,clip=false,trim=0 0 0 0]{figures/power_more_vf.pdf}
    \caption[Execution time and power efficiency ratio with more V/F levels.]{Execution time and power efficiency ratio of HotSpot when varying the number of V/F levels.}
    \label{fig:power_more_vf}
\end{figure}
The number of baseline V/F levels is 10.
To support 12 (or 14) V/F levels, we decrease the minimum operating voltage from 0.72V to 0.69V (or 0.66V) and increase the maximum operating voltage from 0.99V to 1.02V (or 1.05V), which adds two (or four) more V/F levels for DVFS.
We can observe that as the number of supported V/F levels is increased from 10 to 12, the power efficiency of the CPU comes closer to that of the GPU with reduced execution time (i.e., improved overall throughput).
However, increasing the number of V/F levels to 14 does not reduce the execution time further because we have already reached the optimal operating point where the power efficiencies of the CPU and the GPU have been already balanced with the 12 V/F levels.

The key point is that the CPU and the GPU have different performance and power efficiency characteristics that scale differently based on workload characteristics when varying V/F levels, the number of CPU cores, and the number of GPU SMs.
This provides the opportunity to improve the overall throughput of power-constrained SCHPs through DVFCS.
Furthermore, comparing the power efficiency of the CPU and the GPU provides a promising basis for an effective runtime DVFCS algorithm for workload and power budget partitioning.

\section{Runtime DVFCS Algorithm}
\label{sec:power_runtime}
According to the analysis performed in previous section, the basic idea of the runtime DVFCS algorithm is to adaptively adjust the power efficiency between the CPU and the GPU through DVFCS such that the overall throughput is maximized for a given workload.
To develop a runtime DVFCS algorithm, we need to answer the following two questions:
\begin{itemize} \itemsep0in \parskip0in \parsep0in
    \item What percentage of work should we assign to the CPU and the GPU, respectively?
    \item Which knob, DVFS or DCS, should we use first to adjust power efficiency?
\end{itemize}
In this section, we answer these two questions, describe the proposed runtime DVFCS algorithm, and evaluate the effectiveness of the proposed runtime DVFCS algorithm.

\subsection{Optimization Solution Space Analysis}
\label{subsec:power_solutionspace}
First, as shown in Figure~\ref{fig:power_example}, the execution time of both the CPU and the GPU scales linearly with the percentage of work assigned for most benchmarks.
This is because the input data size is large enough; even 6.25\% (1/16) of the work can fully occupy the computing resources and thus its execution time is approximately equal to 6.25\% of the total execution time.

Due to its linear relationship, the optimal workload partitioning point for given V/F levels for the CPU and the GPU, the number of CPU cores, and the number of GPU SMs can be determined after each invocation of a kernel in the main execution loop of the benchmark.
Suppose the current invocation assigns X\textsubscript{i}\% of the work to the GPU and (100-X\textsubscript{i})\% to the CPU, and CPU and GPU execution times are T\textsubscript{GPU} and T\textsubscript{CPU}, respectively.
Then the optimal workload partitioning point for the next invocation (X\textsubscript{i+1}\% of the work on the GPU) can be calculated by simply solving Equation~\ref{eq:workpart}:
\begin{doublespace}
    \begin{equation}
        \mathrm{
            T_{GPU}\cdot{\frac{X_{i+1}}{X_{i}}} =
            T_{CPU}\cdot{\frac{100 - X_{i+1}}{100 - X_{i}}}
        }
        \label{eq:workpart}
    \end{equation}
\end{doublespace}
To obtain X\textsubscript{i+1} as a function of X\textsubscript{i}, T\textsubscript{CPU}, and T\textsubscript{GPU}, Equation~\ref{eq:workpart} can be rearranged as follows:
\begin{doublespace}
    \begin{equation}
        \mathrm{
            X_{i+1} = \frac{T_{CPU}\cdot{X_{i}}}{(T_{CPU} - T_{GPU})\cdot{X_{i}} + T_{GPU}}
        }
        \label{eq:workpart2}
    \end{equation}
\end{doublespace}

Second, both the CPU and the GPU are generally more power efficient when operating at lower V/F.
We plot HotSpot's execution time versus power efficiency ratio for different numbers of CPU cores and GPU SMs in Figure~\ref{fig:power_solspace}.
\begin{figure}[!t]
    \centering
    \subfloat[fix \# of GPU SMs = 12]{
        \centering
        \includegraphics[width=2.95in,clip=false,trim=0 0 0 0]{figures/power_solspace_cpu.pdf}
        \label{subfig:power_solspace_cpu}
    }
    \subfloat[fix \# of CPU cores = 4]{
        \centering
        \includegraphics[width=2.95in,clip=false,trim=0 0 0 0]{figures/power_solspace_gpu.pdf}
        \label{subfig:power_solspace_gpu}
    }
    \caption[HotSpot's execution time versus power efficiency ratio.]{HotSpot's execution time versus power efficiency ratio for different number of CPU cores and GPU SMs.}
    \label{fig:power_solspace}
\end{figure}
Each point represents one valid configuration (i.e., V/F of the CPU and the GPU, the number of CPU cores, and the number of GPU SMs) that satisfies the chip, CPU, and GPU power constraints and delivers higher throughput than the CPU-oriented configuration.
Figure~\ref{fig:power_solspace} demonstrates that using more CPU cores (or GPU SMs) at lower V/F levels generally gives higher throughput than using fewer CPU cores (or GPU SMs) at higher V/F levels.

A similar trend is also observed in all other benchmarks.
Therefore, we use the following strategy for our proposed runtime DVFCS algorithm.
When we need to allocate less power to the CPU (or GPU), we lower its V/F level first.
If the V/F level is already at the lowest possible level but the power efficiency ratio still indicates less power should be assigned to the CPU (or GPU), we begin to turn off CPU cores (or GPU SMs).
Similarly, when we need to allocate more power to the CPU (or GPU), we activate the disabled CPU cores (or GPU SMs) first.
Only after there are no more available CPU cores (or GPU SMs), we begin to increase its V/F if we still need to allocate more power to the CPU (or GPU).
To set the V/F level and the number of CPU cores (or GPU SMs), we apply a binary search method for the given range of V/F levels and the number of available CPU cores (or GPU SMs).
Because the solution points are not very close to the optimal point, it is unlikely that the runtime DVFCS algorithm converges to a local minimum.
Summarizing the above discussion, Figure~\ref{fig:power_algo} shows our runtime DVFCS algorithm.
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_algo.pdf}
    \caption{Runtime DVFCS algorithm.}
    \label{fig:power_algo}
\end{figure}

The DVFCS algorithm, which can be implemented as a part of a runtime system (e.g., the OpenCL runtime layer), can be invoked every time a kernel is launched.
Once the throughput improvement converges, the algorithm no longer changes V/F levels, the number of CPU cores, or the number of GPU SMs.
However, if the algorithm observes a notable throughput change, it will begin to adjust the processor configuration.

\subsection{Evaluation of Runtime DVFCS Algorithm}
\label{subsec:power_runtimeeval}
Figure~\ref{fig:power_tp_runtime} plots the throughput from our cycle-level SCHP simulator for all the benchmarks based on four different configurations: (i) the GPU-oriented configuration, (ii) the configuration selected by an exhaustive search method (denoted by "Optimal DVFCS"), (iii) the configuration selected by running the proposed runtime DVFCS algorithm until it converges (denoted by "Runtime DVFCS"), and (iv) the configuration selected by running runtime DVFCS algorithm for only three iterations (denoted by "Runtime DVFCS-3I").
The optimized configurations selected by runtime DVFCS algorithm leads to 12\% higher throughput on average than the GPU-oriented configuration, while the optimal configurations after exhaustive search is on average 13\% higher; only the results for BKP and SRAD using the proposed runtime DVFCS algorithm are slightly worse than using the exhaustive search.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_tp_runtime.pdf}
    \caption[Throughput improvement with runtime algorithm-controlled DVFCS.]{Throughput comparisons between oracle and runtime algorithm-controlled DVFCS using the detailed cycle-level simulator.}
    \label{fig:power_tp_runtime}
\end{figure}

Our experiment shows that our runtime DVFCS algorithm takes 5 to 8 iterations (6.8 iterations on average) to achieve the optimal DVFCS configuration for 8 out of all 12 workloads.
Moreover, Figure~\ref{fig:power_tp_runtime} shows that significant throughput improvements are achieved after just 3 iterations, with an average throughput improvement of 9.1\%.
This indicates that the overhead of the runtime algorithm is negligible considering that many GPU applications execute the same kernel many times; for example, HotSpot and HeartWall repeat the execution of the same kernel(s) for a specified time window and a specified number of frames, respectively.

We also demonstrate the effectiveness of the proposed optimization technique using a commercial computing system with an AMD Trinity APU \cite{amdtrinity}.
The Trinity APU has (i) 4 CPU cores with four 16KB 4-way associative L1 data caches, two 64KB 2-way associative L1 instruction caches, and a shared 4MB 16-way L2 cache and (ii) 6 GPU compute units (CUs), each of which has 64 stream cores.
We use AMD internal software that enables us to vary V/F of the CPU and the GPU, the number of active CPU cores, and the number of GPU CUs.
We set the nominal frequency for the CPU and the GPU to 3.4GHz and 633MHz, respectively.

Figure~\ref{fig:power_trinity} plots the throughput of all the benchmarks based on four different configurations: (i) CPU-only operating at the maximum V/F level (denoted by "CPU-only (VF\textsubscript{MAX})"), (ii) GPU-only operating at the maximum V/F level (denoted by "GPU-only (VF\textsubscript{MAX})"), (iii) CPU+GPU at the nominal V/F levels using all available CPU cores and GPU CUs (denoted by "CPU+GPU (VF\textsubscript{NOM})"), and (iv) CPU+GPU at the V/F levels determined by DVFCS algorithm.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/power_trinity.pdf}
    \caption[Throughput improvement using AMD Trinity APU.]{Throughput comparisons using a commercial compute system with an AMD Trinity APU.}
    \label{fig:power_trinity}
\end{figure}
The throughput of each configuration is normalized to that of the GPU-only configuration running at the nominal V/F level.
On average, the proposed DVFCS algorithm can provide 35\% higher throughput than the GPU-only configuration operating at the maximum V/F level and 12\% higher throughput than the CPU+GPU configuration operating at the nominal V/F level.
These results confirm our key findings of this study: joint optimization of workload and power budget partitioning through DVFCS can provide higher throughput than the assignment of all the workload to the GPU or partitioning only the workload and using a static power budget allocation.

\section{Summary}
\label{sec:power_summary}
In this chapter, we considered an SCHP, in which the CPU and the GPU have independent V/F domains, and the CPU cores and GPU SMs have independent PG devices, respectively.
Since the CPU and the GPU are in the same chip, they share the chip power budget while the performance of processors is typically limited by power constraint.
Therefore, it requires a judicious and coordinated power management technique for the CPU and the GPU in order to maximize the overall throughput.

In such an SCHP, we first demonstrated that its overall throughput can be significantly improved by jointly optimizing workload and power budget partitioning between the CPU and the GPU.
We performed an exhaustive search of (i) the percentage of work assigned to the CPU and the GPU, (ii) the V/F of the CPU and the GPU, and (iii) the number of active CPU cores and GPU SMs while satisfying chip, CPU and GPU power constraints.
This achieves 13\% throughput improvement over the SCHP configuration operating both the CPU and the GPU at the nominal V/F levels.
Second, we proposed an effective runtime DVFCS algorithm that compares the power efficiency of the CPU with that of the GPU for a given workload and power budget partitioning.
Our algorithm can find an optimal or near-optimal configuration within 5$\sim$8 iterations while most of the kernels in the workloads we evaluated are often executed more than a thousand times.
Finally, we confirm the key findings of this study using a commercial computing system with an AMD Trinity APU.

