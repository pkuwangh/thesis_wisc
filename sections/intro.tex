\chapter{Introduction}
\label{ch:introduction}
With aggressive technology scaling, more computing elements are integrated in a processor chip over technology generations. 
Due to the end of Dennard's scaling \cite{powerwall} and the increasing processor-memory performance gap \cite{memorywall}, power consumption and memory system performance have become two critical constraints that limit the performance of computing systems.
Furthermore, the integration of the CPU and the GPU in the same chip introduces new challenges in two aspects: (i) requiring judicious management of on-chip resources shared between the CPU and the GPU; and (ii) requiring innovation in memory architecture to adapt to the diverse memory access characteristics of the CPU and the GPU.
In this thesis, I address the specific challenges in the above two aspects, with two research projects in each aspect.

\section{Technology Trends}

\subsection{Heterogeneous Processors}
Technology scaling has been a fundamental driving force for the continuously growth in computing power \cite{mooreslaw}.
Traditionally, growth in computing power was realized by (i) increasing clock frequency with faster transistors and deeper pipelining, and (ii) improving instruction-level parallelism with microarchitecture innovations, until the power wall was hit \cite{powerwall}.
As single-thread performance began to be limited by power constraint, instead of using additional transistors to build more sophisticated microarchitecture, chip multiprocessors (CMPs) were introduced to utilize the transistors more efficiently.
Beyond quad-core or octa-core processors, rather than doubling the number of replicated cores every technology generation, manufacturers began to integrate heterogeneous computing elements, i.e. the CPU and the GPU, in the same chip.
The CPU is designed for high single-thread performance with sophisticated out-of-order execution and aggressive speculation, while the GPU is designed for high throughput with massive multi-threading.
Recent examples include AMD's Llano \cite{amdllano}, Trinity \cite{amdtrinity}, Kaveri \cite{amdkaveri} processors, and Intel's Sandy Bridge \cite{intelsandybridge}, Ivy Bridge \cite{intelivybridge}, Haswell \cite{intelhaswell} processors.

\subsection{Power Consumption}
Due to the non-ideal characteristics of nano-scale transistors, the threshold voltage (V\textsubscript{th}) has not scaled down as expected, in order to prevent substantial leakage power increase.
Consequently, the supply voltage (V\textsubscript{dd}) of the integrated circuits (ICs) has also scaled much slower than the feature size of transistors, in order to deliver the desired performance improvement.
As a result, the power density of microprocessor chips has increased significantly over technology generations \cite{vddscaling}.
Therefore, the power constraint has become a chief design constraint \cite{powerwall}.

\subsection{Memory Latency and Bandwidth}
DRAM has been used for the main memory system for decades due to its high density and performance.
In the evolution of DRAM technology, the demand for high capacity and low cost per bit has been the key driving force.
With continuous technology scaling, the single-chip DRAM capacity has increased dramatically.
However, due to the tight cost constraint, the faster transistors have been exploited mainly to increase the density instead of decreasing the access latency \cite{tieredlatency}.
The internal clock rate for DDR SDRAM is 100-200MHz, and it is still only 100-267MHz for DDR3 and DDR4 SDRAM \cite{specddr1234}.
Similarly, the access time is 35ns for DDR-400C, down to 30ns for DDR2-800C, 27.5ns for DDR3-1600K.
In terms of CPU cycles, the DRAM access latency has been even growing considering the increasing CPU frequency.

On the other hand, the memory bandwidth has gone through dramatic increase thanks to the introduction of synchronous interface, improved concurrency with multiple banks, higher I/O frequency, and increased parallelism with multiple channels.
In particular, the last two factors could continuously increase bandwidth.
According to JEDEC, the memory bandwidth has increased $\sim$100$\times$ over the past decades \cite{memoryroadmap}.
However, neither the data bus width (i.e., number of channels) nor the I/O speed is expected to scale well in future with current memory interface.

\section{Challenges}
\label{sec:intro_challenges}

\subsection{Resource Management for Heterogeneous Processors}
\label{subsec:intro_resource}
In a single-chip heterogeneous processor, various system resources are shared between the CPU and the GPU, such as on-chip interconnect network, shared last-level cache, off-chip memory bandwidth, and chip power budget.
As explained in the above section, the power budget and memory bandwidth usually limit the performance and hence require judicious management.

First, the CPU and the GPU share a chip-level total power budget.
The power budget allocation between the CPU and the GPU can be realized by dynamically scaling the voltage/frequency of a core and the number of operating cores.
In addition, the CPU and the GPU have different power and performance characteristics.
As a result, the heterogeneous architectures (i.e., CPU and GPU), the independent control of voltage/frequency of CPU and GPU, and the ability to turn off certain cores, together create a large multi-dimensional optimization space to improve the overall throughput of an SCHP.

Second, the integration of the GPU in the same chip with the CPU introduces additional challenges since GPU applications typically demand considerably higher memory bandwidth than CPU applications.
The GPU maintains a large number of hardware thread contexts.
Therefore, when a thread is stalled due to a long-latency memory access, the execution can switch to other ready threads with very low switching overhead.
As a result, the GPU can issue many outstanding memory requests and tolerate the long memory latency to certain extent.
In contrast, the CPU typically does not consume as much bandwidth as the GPU, but it is very sensitive to the memory latency.
Therefore, the memory scheduling policy should be aware of such differences in memory characteristics and demands between the CPU and the GPU.

\subsection{Design of Heterogeneous Memory Systems}
\label{subsec:intro_memory}
Section~\ref{subsec:intro_resource} describes the challenges to improve the utilization of existing bandwidth.
However, with the ever-increasing bandwidth demand, especially by the GPU in an SCHP, the peak bandwidth that can be achieved with existing memory interface will become insufficient.
The key problem with current parallel interface is that over-extending the channel speed degrades the energy efficiency of I/O interface super-linearly, resulting in exploding I/O power consumption to achieve high bandwidth \cite{iotutorial}.
Alternatively, recent serial interfaces have demonstrate superior energy efficiency at very high frequency, while the critical latency is notably higher than current parallel interface.
Therefore, a heterogeneous memory system with both parallel and serial channels has the potential to provide both low latency and high bandwidth.

However, heterogeneous memory systems inevitably require smart page placement mechanisms.
Since different memory regions exhibit different characteristics, a page should be placed in a specific memory region that matches its access pattern.
For example, a memory device with asymmetric organization can have a fast, small region and a slow, large region.
Then the frequently-accessed memory pages (i.e., hot pages) should be placed in the fast region, such that most memory requests can be serviced by the fast region.
Considering the page placement decision is made in advance before the data can be used, such a smart page placement mechanism can be very challenging to implement.
Therefore, a low-overhead page migration mechanism that can move pages efficiently between the fast and slow regions will be highly desirable.

\section{Contributions}
\label{sec:contribution}
\subsection{Workload and Power Budget Partitioning}
I first demonstrated that joint optimization of workload and power budget partitioning between the CPU and the GPU results in significantly higher throughput than optimization of workload partitioning alone with fixed power budget allocations.
Second, I developed a runtime algorithm that can determine near-optimal or optimal power budget partitioning within a small number of kernel iterations.
Finally, I demonstrated the effectiveness of the proposed optimization technique using both (i) an integrated gem5+GPGPU-Sim simulator and (ii) a real computing system with an AMD Trinity processor.

\subsection{Memory Scheduling}
I first demonstrated that recent memory scheduling techniques proposed for heterogeneous processors actually perform notably worse than conventional first-ready, first-come-first-service policy in cooperative heterogeneous computing scenario, in which a single parallel workload is partitioned to run on both the CPU and the GPU.
Second, I performed a comprehensive analysis on the memory characteristics and access patterns in cooperative heterogeneous computing scenario.
Based on the analysis, I proposed a novel memory scheduler that can improve the overall throughput of SCHPs in this scenario.

\subsection{Hybrid Memory Channel Architecture}
I first provided a deep understanding of recent high-speed serial interface (HSI), including its distinct characteristics compared to past serial interfaces, how it works as a memory interface, and how it impacts the main memory system design.
To tackle the bandwidth-wall challenges, I proposed hybrid memory channel architecture comprised of parallel and serial channels.
Considering that typically the GPU is sensitive to bandwidth while the CPU is sensitive to latency, I proposed a memory channel partitioning technique that adaptively maps physical pages of latency-sensitive (CPU) and bandwidth-consuming (GPU) applications to low-latency parallel and high-bandwidth serial channels, respectively.
The proposed hybrid channel architecture can improve the performance of both the CPU and the GPU significantly.
Finally, I also provided a detailed analysis on the peak power and energy consumption of HSI-based memory channels.

\subsection{Lightweight Page Migration}
I proposed a novel row-buffer architecture that shares a set of row buffers between two neighboring banks.
With this shared row-buffer architecture, I developed an efficient way to migrate a page between two neighboring banks, which can be asymmetric (i.e., a fast, low-capacity bank and a slow, high-capacity bank).
This lightweight page migration mechanism can move frequently-accessed pages to the fast bank, such that most of memory accesses are serviced by fast banks.
In contrast to existing asymmetric memory architecture, our approach allows almost free page migration as it does not need to either explicitly transfer pages or hold a memory channel during the transfers.

\section{Thesis Organization}
This thesis is organized as follows.
Chapter~\ref{ch:background} provides a detailed overview of background and previous work related to heterogeneous processors, power management techniques, memory architecture, and memory scheduling techniques.
Chapter~\ref{ch:methodology} describes the experimental methodology, including performance simulation infrastructure, evaluated benchmarks, and power modeling method.
Chapter~\ref{ch:power} presents our power management technique when partitioning a parallel workload on both the CPU and the GPU for SCHPs.
Chapter~\ref{ch:memsched} presents our study on memory scheduling for SCHPs.
Chapter~\ref{ch:hsi} describes our hybrid memory channel architecture that uses both parallel and serial channels.
Chapter~\ref{ch:pcm} describes our shared row-buffer architecture for phase-change memory to support a lightweight page migration mechanism.
Chapter~\ref{ch:conclusion} summarizes the thesis and future research directions.

