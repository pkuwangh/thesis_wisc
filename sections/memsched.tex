\chapter{Memory Scheduling for Heterogeneous Processors}
\label{ch:memsched}

Portions of this chapter were previously published as \cite{wang2014pact} in ACM 23rd international conference on parallel architectures and complication techniques in 2014.

\section{Introduction}
\label{sec:memsched_intro}
As discussed in Section~\ref{sec:mem_sched}, previous studies on memory scheduling for single-chip heterogeneous processors (SCHPs) have focused on a multi-tasking scenario where the GPU and the CPU are running graphics and multiple general-purpose applications, respectively.
However, there exists another increasingly important usage scenario of SCHPs, where a single parallel application is partitioned between and executed on both the CPU and the GPU, as described in Section~\ref{subsec:opencl_chc}.
This cooperative heterogeneous computing approach utilizes both the CPU and the GPU as compute devices to maximize the overall throughput.
It was reported that a cooperative heterogeneous computing approach using an Intel i7-3770K processor provided 2.55$\times$ and 1.89$\times$ higher throughput than using only the CPU or the GPU for two industry benchmarks, LuxMark and Sandra, respectively \cite{intelopencl}.

Under such a specific yet important computing model, an alternative guideline for designing memory scheduling techniques is needed due to following two reasons.
First, the evaluation metric for cooperative heterogeneous computing is fundamentally different from that for multi-tasking.
When multiple applications are running on a processor, fairness is a key concern for developing memory scheduling techniques.
Some of the previous studies also attempt to improve overall system performance; however, as explicitly mentioned in \cite{sms}, it is usually not straightforward to define the system performance in an SCHP running multiple applications.
In contrast, when we consider a cooperative heterogeneous computing scenario, the design objective is clearly to maximize overall throughput.
Furthermore, we observe that guaranteeing fairness between the CPU and the GPU becomes less meaningful and such an attempt often degrades the overall throughput.
For example, if a given application runs more efficient on the GPU than the CPU, more computation will be allocated to the GPU.
In this circumstance, enforcing fairness (e.g., minimizing maximum slowdown of the CPU \cite{fairness, atlas, sms}) can degrade overall throughput.

Second, memory-intensive applications are likely to substantially degrade the performance of less memory-intensive applications with application-oblivious memory scheduling policies.
This is one of the key challenges that previous studies on memory scheduling attempted to tackle \cite{fqms, parbs, atlas, tcm, sms}, as discussed in Section~\ref{sec:mem_sched}.
One of the key principles in previous memory scheduling techniques is to prioritize the requests from non-memory-intensive applications over memory-intensive ones.
This intuitively makes sense since prioritizing requests from non-memory-intensive applications substantially improve their performance without a notable negative impact on other memory-intensive ones; non-memory-intensive ones will not consume much bandwidth even with high priority.
In contrast, in a cooperative heterogeneous computing scenario, the CPU and the GPU will show relatively similar memory access characteristics, since essentially it is a single application running on both sides.
For instance, an application that is inherently memory-intensive will generate intensive memory requests from both sides, although the access patterns and intensities will be different due to the asymmetries in core frequency, the number of hardware threads, and on-chip cache hierarchy.
Therefore, the memory controller's awareness of the difference in application characteristics will lose its effectiveness in the context of single-chip cooperative heterogeneous computing.

\section{Experimental Methodology}
\label{sec:memsched_methodlogy}

\subsection{System Configuration}
\label{subsec:memsched_config}
In this study, we aim to model a next-generation heterogeneous processor, similar to an AMD APU yet with more CPU cores and GPU SMs.
Table~\ref{tab:memsched_config} summarizes the key configuration parameters of the evaluated system.
\begin{table}[!b]
    \caption{System configurations.}
    \centering
    \begin{tabular}{|c||c|}
        \hline
        \multirow{3}{*}{\textbf{CPU}} & 8 cores, 4.0GHz, 4-wide issue \\
                                      & 2-way associative, 32KB private I-L1\$ \& D-L1\$ \\
                                      & 16-way associative, 4MB shared L2\$ \\
        \hhline{|=||=|}
        \multirow{4}{*}{\textbf{GPU}} & 16 SMs, 800MHz \\
                                      & 32 SIMD lanes / 16384 registers per SM \\
                                      & max 1024 threads / 8 CTAs per SM \\
                                      & 32KB on-chip memory \\
        \hhline{|=||=|}
        \multirow{4}{*}{\textbf{Memory}} & 2 channels, 2 ranks/channel, 8 banks/rank \\
                                         & 800MHz DRAM I/O freq, 64 bits/channel \\
                                         & tCAS/tRCD/tRP = 13.75/13.75/13.75 ns \\
                                         & tRAS/tRC = 35/48.75 ns \\
        \hline
    \end{tabular}
    \label{tab:memsched_config}
\end{table}

\subsection{FR-FCFS, SMS and FQMS}
\label{subsec:memsched_smsfqms}
We implemented three existing or recently proposed memory scheduling techniques.
FR-FCFS is a conventional scheduling policy that prioritizes requests resulting in row-buffer hit \cite{frfcfs}.
It maximizes the utilization of memory system, while it does not exploit any application characteristics.
As a result, when multiple applications are running simultaneously, a memory-intensive application with high spatial locality is likely to significantly degrade the performance of non-memory-intensive applications.
Furthermore, the integration of a GPU on the same die with a CPU could further worsen the situation since graphics applications typically generate excessive memory requests which exhibit high spatial locality.

Recent studies proposed various memory scheduling techniques to guarantee the fairness and provide QoS to each application running in the shared-memory system.
SMS refers to staged memory scheduling, which is a novel memory controller design to improve the performance, fairness, and scalability in SCHP systems \cite{sms}.
It decouples the primary functions of a memory controller in three stages.
In the first stage, it groups memory requests into batches per source (a CPU core or the GPU) FIFO queues to preserve row-buffer locality.
The second stage performs batch scheduling across applications to improve fairness.
The batch scheduler uses a short-job-first (SJF) policy with a probability of \textit{p}, and a round-robin policy otherwise.
\textit{p} is a tunable parameter that can be used to adjust the priorities of the CPU and the GPU.
We sweep the value of \textit{p} from 0 to 1 with a step size of 0.1 and take the best result.

Our FQMS implementation adapts a fair queuing memory system for SCHP systems \cite{fqms}.
The basic approach is to guarantee each core/thread receives its allocated share of memory bandwidth, and therefore provide QoS to all threads.
Each incoming request is annotated with virtual start and finish times.
Virtual times are calculated based on the arrival time, the virtual finish time of the previous request to the same bank, the service time of the request, and the bandwidth share allocated to the thread.
The scheduler prioritizes requests based on the earliest virtual finish time, because it is the deadline to offer QoS.
Similarly, we sweep the bandwidth partitioning point from 0.1 versus 0.9 to 0.9 versus 0.1 at the granularity of 0.1 and take the best result.

\section{Memory Characteristics of Cooperative Heterogeneous Computing}
\label{sec:memsched_pattern}
We gather the memory access characteristics of ten GPU benchmarks for a cooperative heterogeneous computing scenario.
Figure~\ref{fig:memsched_mem_intensity} presents the memory request intensities of the CPU and the GPU, when each application is running alone in the system.
The throughput ratio of the GPU over the CPU running the same benchmark is also plotted in Figure~\ref{fig:memsched_mem_intensity}.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_mem_intensity.pdf}
    \caption{Memory intensity of parallel benchmarks running on the CPU and the GPU.}
    \label{fig:memsched_mem_intensity}
\end{figure}
All ten benchmarks in Figure~\ref{fig:memsched_mem_intensity} are categorized into two groups.
In group (ii), the CPU generates much fewer requests than the GPU, because a large L2 cache of the CPU filters most CPU memory requests for these benchmarks.
However, most benchmarks are categorized in group (i), which exhibit comparable memory intensities (i.e., memory requests per kilo memory cycles) between the CPU and the GPU because fundamentally it is the same application that runs on both the CPU and the GPU.
In contrast, in a multi-tasking scenario, one or a mix of SPEC CPU2006 benchmarks may run on the CPU and a graphics or GPGPU application may run on the GPU.
In such a case, the memory access characteristics of the CPU and the GPU can be substantially diverse.

In order to depict the distinction between multi-tasking and cooperative heterogeneous computing scenario in memory characteristics, we show three examples in Figure~\ref{fig:memsched_mem_char}.
\begin{figure}[!b]
    \centering
    \subfloat[]{
        \centering
        \includegraphics[width=2.8in,clip=false,trim=0 0 0 0]{figures/memsched_char_mt.pdf}
        \label{subfig:memsched_mem_char_a}
    }
    \qquad
    \subfloat[]{
        \centering
        \includegraphics[width=2.8in,clip=false,trim=0 0 0 0]{figures/memsched_char_bkp.pdf}
        \label{subfig:memsched_mem_char_b}
    }
    \subfloat[]{
        \centering
        \includegraphics[width=2.8in,clip=false,trim=0 0 0 0]{figures/memsched_char_sc.pdf}
        \label{subfig:memsched_mem_char_c}
    }
    \caption[Memory characteristics in cooperative heterogeneous computing scenarios.]{Memory characteristics in multi-tasking scenario (a) and cooperative heterogeneous computing scenario (b, c).}
    \label{fig:memsched_mem_char}
\end{figure}
We gather the memory requests per kilo memory cycles (MPKC), row-buffer locality (RBL) and bank-level parallelism (BLP) for different applications.
Figure~\ref{subfig:memsched_mem_char_a} describes a multi-tasking scenario that has one GPU application (Back Propagation) running on the GPU and multiple SPEC CPU2006 applications running on the CPU.
Since essentially the executed applications are dissimilar, there is no correlation in memory characteristic between the GPU and CPU applications.
Especially the memory intensity of CPU applications is far lower than the GPU application.
In contrast, Figure~\ref{subfig:memsched_mem_char_b} and Figure~\ref{subfig:memsched_mem_char_c} represent cooperative heterogeneous computing scenarios that have one single parallel application running on both the CPU and the GPU.
As we can see, there is a clear similarity between the CPU and the GPU in memory characteristics.

This is one of the key observations that motivate us to re-consider the existing memory scheduling algorithm.
Previous studies on application-aware memory schedulers for a multi-tasking scenario attempt to prevent the interference from memory-intensive applications, especially those with high row-buffer locality.
The basic approach to tackle this problem is to always prioritize requests from non-memory-intensive applications over memory-intensive ones.
The rationale behind this approach is that prioritizing requests from non-memory-intensive applications can substantially improve the performance of these applications, without degrading the performance of memory-intensive applications, because the prioritized applications only consume trivial bandwidth.
Consequently, the effectiveness of such a scheduling principle relies on the substantial distinction in memory characteristics among applications.
However, when a single parallel application running on an SCHP, we will see similar memory characteristics between CPU and GPU requests.

Figure~\ref{fig:memsched_pattern_mt} presents the access pattern of a mix of 4 SPEC CPU2006 benchmarks (i.e., multi-tasking scenario) with diverse levels of memory intensities.
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_pattern_mt.pdf}
    \caption[Memory access pattern for a mix of 4 SPEC CPU2006 applications.]{Memory access pattern for a mix of 4 SPEC CPU2006 applications running on a 4-core CPU. The x-axis represents the time a request arrives at memory controller; the y-axis represents the bank index of the requests.}
    \label{fig:memsched_pattern_mt}
\end{figure}
We track the arrival time of each memory request and sort all the requests based on the bank index that they access in one of the dual memory channels.
As we can observe, the memory access pattern depicted in Figure~\ref{fig:memsched_pattern_mt} are fairly random, especially for namd and bzip2.
Therefore, the useful information we can obtain from the access pattern is very limited

In contrast, Figure~\ref{fig:memsched_pattern_chc} plots the memory access pattern of a parallel benchmark, Back Propagation, running in an SCHP system.
The access pattern shown in Figure~\ref{fig:memsched_pattern_chc} clearly exhibits regularity.
The requests appear as separate periodical strips for the CPU and the GPU.
\begin{figure}[!b]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_pattern_chc.pdf}
    \caption[Memory access pattern for Back Propagation.]{Memory access pattern for Back Propagation running both the CPU and the GPU.}
    \label{fig:memsched_pattern_chc}
\end{figure}
Due to the dissimilarity in core frequency and the number of hardware threads, the recurrent CPU strips have a different period and slope from the GPU strips, leading to periodic overlaps with each other, indicating the CPU and the GPU are accessing the same bank in the same period of time.
This is where the interference happens and the potential row-buffer conflict penalty is paid.
Previous studies proposed to (i) group requests into batches based on the requesting application and the accessing row, and (ii) schedule the batches of requests based on row-buffer locality \cite{sms}.
However, the batch formation introduces additional latency which can be undesirable for high-throughput computing.
Instead, we can exploit the regularity in access patterns of these data-parallel applications, and thus apply specific optimizations.
More specifically, memory requests from the CPU to a certain bank are fairly concentrated since the CPU maintains much fewer active hardware threads than the GPU.
Further, the bank access sequences are highly predictable.
Therefore, we can simply apply a lock on the bank that the CPU is accessing for a short period of time to preserve the row-buffer locality.

\section[Memory Scheduling for Cooperative Heterogeneous Computing]{Memory Scheduling for High-Throughput Cooperative Heterogeneous Computing}
\label{sec:memsched_sched}

\subsection{Memory Controller Organization}
\label{subsec:memsched_sched_mc}
Figure~\ref{fig:memsched_mc} illustrates a typical design of a memory controller.
\begin{figure}[!b]
    \centering
    \includegraphics[width=3.2in,clip=false,trim=0 0 0 0]{figures/memsched_mc.pdf}
    \caption{Memory controller organization.}
    \label{fig:memsched_mc}
\end{figure}
First, the CPU and the GPU have its own in-order request buffer.
An input arbitration is performed every cycle to determine which side will push a request into the front-end scheduling buffer, to which a scheduling algorithm is applied.
This front-end scheduling buffer is fairly small since it is impractical to have a large buffer with sophisticated out-of-order scheduling logic enforced \cite{sms}.
When a request is scheduled for a service, it is popped up from the front-end buffer, and pushed into the back-end per-bank command queue.
At the back-end, it tracks the state of each DRAM bank, and manages various timing constraints.
Note that the back-end command queue only has one entry per bank, in order to provide simple and straightforward information to the front-end scheduling logic.
Finally, bus arbitration is performed among the banks to choose a ready command to be issued to a DRAM channel.

\subsection{Scheduling Techniques}
\label{subsec:memsched_sched_tech}
The key concept of our proposed scheduling techniques consider two aspects:
(i) since the CPU is more latency-sensitive, we attempt to reduce the average latency of CPU requests as much as possible by giving CPU requests high priority, yet without explicitly constraining GPU requests;
(ii) we need a parameter that can adjust the degree of prioritization in a fine-grain manner to accommodate various workload partitioning points and thus different relative importance between the CPU and the GPU.

\subsubsection{Prioritization of CPU Requests}
First, we enable the prioritization of CPU requests at various stages in the memory controller depicted in Figure~\ref{fig:memsched_mc}.
At the input arbitration stage, requests from the CPU buffer can be prioritized;
at the front-end scheduling queue, beyond the commonly used FR-FCFS policy, the CPU requests can be prioritized such that an old CPU request receives higher priority than a ready GPU request;
At the back-end stage, the bus arbitration can also prioritize CPU requests over GPU requests that are arbitrated in a round-robin fashion.

\subsubsection{Fine-Grain Adjustment of Prioritization}
As mentioned earlier, the relative importance of CPU and GPU performance depends on how given workload is partitioned between the CPU and the GPU.
For example, assume 80\% of the work is assigned to the GPU.
Even if CPU performance is significantly degraded, it may still not be desirable to highly prioritize CPU requests.
This is because the GPU may contribute more to the overall throughput while giving unnecessarily high priority to CPU requests often hurts GPU performance.
Considering the workload partitioning point varies across benchmarks and architectures of the CPU and the GPU, it is desirable to have a parameter that can adjust the degree of prioritization in a fine-grain manner.
Therefore, we introduce a prioritization threshold value (\textit{P\textsubscript{th}}) that decides the fraction of CPU requests that are prioritized.
When a CPU request enters a memory controller, we attach one bit to the request to indicate whether or not the request is prioritized.
The scheduling logic will only enforce the aforementioned prioritization techniques when the CPU request is tagged as a prioritized one.

\subsubsection{Adjusting \textit{P\textsubscript{th}} at Runtime}
Previous studies have been using software-programmable configuration parameters to dynamically adjust the priority at runtime \cite{fqms, sms}.
Nevertheless, such an adjustment at runtime is always challenging, because the optimal parameters depend on the characteristics and performance requirement of a given application at runtime.
Especially in a multi-tasking scenario, it may require the system to determine which application is unexpectedly slowed down by the system itself, which may further require either an off-line profiling or predicting technique.
However, in the context cooperative heterogeneous computing, determining such parameters can be simple and straightforward.
As mentioned earlier, these data-parallel applications generally have many iterations (e.g., 2000 iterations for CFD by default in original code).
The runtime system can gather the performance statistics (i.e., kernel execution time) after each iteration, and then it can iteratively search for better performance.
As an example, we can start from \textit{P\textsubscript{th}} = 0; then after each iteration, we increase the \textit{P\textsubscript{th}} by 0.1 to see if we obtain a better result (i.e., shorter execution time of one iteration).
Therefore, within no more than ten iterations, we can find the optimal \textit{P\textsubscript{th}}.

\subsubsection{Adaptive Bank Lock}
While exploring our prioritization technique, we observe that simply prioritizing CPU requests at the front-end scheduling buffer is not effective enough.
For example, when a CPU request arrives at a memory controller, ideally it is expected to be serviced immediately considering its high priority.
However, another GPU request to the same bank may have been already scheduled before the CPU request actually enters the scheduling buffer.
Consequently, the CPU request has to wait for tRAS even it has a higher priority.
Further, we discover that such a case occurs frequently due to the high memory intensity and bank-level parallelism of GPU requests, as indicated in Figure~\ref{fig:memsched_mem_intensity} and Figure~\ref{fig:memsched_mem_char}.
Therefore, we propose to lock a bank in advance, which prevents any GPU requests from being scheduled to the corresponding bank.
Consequently, the bank stays idle even with pending GPU requests to the bank in buffer, and thus the bank is ready to service a CPU request as soon as the CPU request arrives.
To support such a lock technique, we maintain one counter, which serves as a lock, for each bank at the front-end stage.
If the counter of a certain bank has a non-zero value, the scheduling logic will not schedule a GPU request to that bank.
Let's say the current counter value is zero and a CPU request is issued to the corresponding bank.
This sets the counter value to a lock cycle value (\textit{N\textsubscript{LC}}).
Afterwards the counter value is decremented every cycle, indicating that the lock will be effective for the next \textit{N\textsubscript{LC}} cycles.

\subsubsection{Adjusting \textit{N\textsubscript{LC}} at Runtime}
We need to ensure that the lock lasts long enough to cover subsequent CPU requests to the same bank while minimizing the negative impact on GPU throughput.
This requires us to adapt \textit{N\textsubscript{LC}} dynamically at runtime, because memory access characteristics and patterns vary from one application to another, as well as from one interval to another and from one partitioning point to another for a given application.
We have another counter for each bank to periodically track the time interval between the first and last requests to certain bank within a time window (e.g., 200 memory cycles), which becomes the basis to determine the \textit{N\textsubscript{LC}} value for the next interval and is denoted as the expected \textit{N\textsubscript{LC}} value (\textit{N\textsubscript{LC\_EXP}}).
We update \textit{N\textsubscript{LC}} by taking the average of current \textit{N\textsubscript{LC}} and \textit{N\textsubscript{LC\_EXP}} values for better stability when adjusting the \textit{N\textsubscript{LC}} value at runtime.
We also apply an upper limit on \textit{N\textsubscript{LC}} to prevent unexpectedly long lock cycles.

\section{Performance Evaluation}
\label{sec:memsched_perf}

\subsection{Effectiveness of Bank Lock Scheme}
\label{subsec:memsched_perf_lock}
Figure~\ref{fig:memsched_perf_lock} presents the effectiveness of the adaptive bank lock scheme described above in Section~\ref{subsec:memsched_sched_tech}.
In order to demonstrate the effectiveness of dynamically adjusting a lock cycle value (\textit{N\textsubscript{LC}}), we measure the percentage of CPU requests that are covered by bank locks (i.e., bank lock coverage).
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_lock.pdf}
    \caption[Effectiveness of our adaptive bank lock technique.]{Effectiveness of our adaptive bank lock technique and the impact on GPU throughput. "Bank lock coverage" represents the percentage of CPU requests covered by applying bank locks; GPU throughput is normalized to the baseline FR-FCFS case. "Optimal fixed lock cycle" sweeps \textit{N\textsubscript{LC}} and chooses the best result; "Adaptive lock scheme" uses the runtime technique to decide \textit{N\textsubscript{LC}} as described in Section~\ref{subsec:memsched_sched_tech}.}
    \label{fig:memsched_perf_lock}
\end{figure}
As a static oracle case, we sweep the \textit{N\textsubscript{LC}} value over multiple runs and choose the best result.
On average, the bank lock with the optimal fixed \textit{N\textsubscript{LC}} value covers 80\% of the CPU requests.
In comparison, our adaptive technique that adjusts the \textit{N\textsubscript{LC}} value dynamically at runtime achieves on average 76\% coverage, indicating our adaptive bank lock technique can effectively cover a large fraction of the CPU requests.

Our simulation results also suggest that the CPU requests, which are covered by bank locks, demonstrate significant queuing latency reduction at the front-end stage (on average 51\%) compared to the case that only high priority is applied to CPU requests for scheduling memory requests.
In contrast, the GPU throughput degradation introduced by bank lock is only 3\% on average, as shown in Figure~\ref{fig:memsched_perf_lock}.

\subsection{Row-Buffer Locality and Latency Analysis}
\label{subsec:memsched_perf_rbllat}
Figure~\ref{fig:memsched_perf_rbl} shows the row-buffer locality of memory requests for the CPU and the GPU, respectively.
First, SMS has lower row-buffer locality on both the CPU and the GPU.
The reason is that although it uses batch scheduling to preserve the locality, its in-order scheduling imposes a fundamental limit on the available locality.
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_rbl.pdf}
    \caption[Row-buffer locality for SMS, FQMS, FR-FCFS and CHC-MS.]{Row-buffer locality of CPU and GPU requests for SMS, FQMS, FR-FCFS and CHC-MS. Row-buffer locality values are measured by the fraction of accesses that hit in the row buffer, and are normalized to FR-FCFS case.}
    \label{fig:memsched_perf_rbl}
\end{figure}
Second, FQMS performs poorly in terms of row-buffer locality, especially for the CPU.
This is related to its original design target.
FQMS is designed to provide QoS for all threads.
Therefore, it schedules the requests strictly based on their virtual finish time such that it can even hurt the inherent locality.
Finally, our proposed scheme targeting cooperative heterogeneous computing (CHC-MS) improves the row-buffer locality by 10\% on average, indicating the effectiveness of our bank lock technique.
In addition, the row-buffer locality of GPU requests is also slightly higher than FR-FCFS, because our bank lock technique helps to reduce the interference between CPU and GPU requests.

Figure~\ref{fig:memsched_perf_latency} shows the average latency of memory requests for the CPU and the GPU, respectively.
First, SMS has higher average latency than FR-FCFS on both the CPU and the GPU.
SMS has a stage of batch formation, where an incoming request has to wait in the buffer until the subsequent requests from the same source to form a batch.
While this method helps to preserve the row-buffer locality, it also inevitably introduces additional queuing latency.
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_latency.pdf}
    \caption[Average latency for SMS, FQMS, FR-FCFS and CHC-MS.]{Average latency of CPU and GPU requests for SMS, FQMS, FR-FCFS and CHC-MS. CAS latency and service latency are excluded since they are constant. Latency values are normalized to FR-FCFS case.}
    \label{fig:memsched_perf_latency}
\end{figure}
Second, compared to FR-FCFS, FQMS shows lower latency for CPU requests.
This matches the design objective of FQMS.
CPU requests are probably delayed in FR-FCFS, while FQMS attempts to guarantee the fairness and provides QoS to both the CPU and the GPU.
As a result, GPU performance is sacrificed to improve CPU performance.
However, in fact, the GPU performance is more important as it contributes more to the overall throughput.
Finally, our CHC-MS reduces the latency of CPU requests by 26\% while only increasing the latency of GPU requests by 7\%.

\subsection{Throughput Analysis}
\label{subsec:memsched_perf_tp}
Figure~\ref{fig:memsched_perf_both} presents the throughput with different degrees of prioritization for the CPU and the GPU, respectively.
The \textit{P\textsubscript{th}} parameter decides the percentage of prioritized CPU requests.
As we prioritize more CPU requests, the throughput of CPU increases significantly, while GPU throughput is inevitably degraded, yet at a lower rate than the improvement of the CPU.
\begin{figure}[!t]
    \centering
    \subfloat[CPU]{
        \centering
        \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_cpu.pdf}
        \label{subfig:memsched_perf_cpu}
    }
    \qquad
    \subfloat[GPU]{
        \centering
        \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_gpu.pdf}
        \label{subfig:memsched_perf_cpu}
    }
    \caption[Throughput of the CPU and the GPU with different \textit{P\textsubscript{th}}.]{Throughput of the CPU and the GPU with different prioritization threshold (\textit{P\textsubscript{th}}). Throughput values of the CPU and the GPU are normalized to CPU-only and GPU-only cases, respectively.}
    \label{fig:memsched_perf_both}
\end{figure}
This is because (i) the GPU is able to hide the memory latency to certain extent while the CPU is very sensitive to the long latency and thus the prioritization of CPU requests will help the CPU more and hurt the GPU less; and (ii) the bank lock scheme we use only rejects GPU requests (i.e., lock the bank for the CPU) for a short period of time, and hence it minimizes the negative impact on GPU performance while preserving the row-buffer locality of CPU requests.
However, it may not be desirable to always prioritize CPU requests since the GPU may contribute more to the overall throughput depending on the optimal workload partitioning point.
Therefore, tunable \textit{P\textsubscript{th}} enables a large optimization space to accommodate different workload partitioning points and hence the varying relative importance of the CPU and the GPU.

Figure~\ref{fig:memsched_perf_system} presents the overall throughput of SMS, FQMS, FR-FCFS, and our proposed CHC-MS.
All throughput values are normalized to FR-FCFS case.
We also plot the optimal degree of prioritization (\textit{P\textsubscript{th}}) that gives the best result for CHC-MS.
\begin{figure}[!t]
    \centering
    \includegraphics[width=6.0in,clip=false,trim=0 0 0 0]{figures/memsched_perf_system.pdf}
    \caption[Overall throughput of SMS, FQMS, FR-FCFS, and CHC-MS.]{Overall throughput of SMS, FQMS, FR-FCFS, and CHC-MS, and the optimal \textit{P\textsubscript{th}} giving the best result for CHC-MS.}
    \label{fig:memsched_perf_system}
\end{figure}
First of all, we find the conventional FR-FCFS policy, which has been criticized for use in CMPs or SCHPs, actually performs better than SMS and FQMS by 10\% and 6\%, respectively.
An intuitive explanation is that it is one single application running on both the CPU and the GPU, which is similar to the conventional uniprocessor case, and therefore FR-FCFS is preferred due to its capability to provide high bus utilization.
Finally, our scheme (CHC-MS) achieves up to 8\% and on average 3\% additional throughput improvement over FR-FCFS, which is 13\% higher than SMS and 10\% higher than FQMS.

\section{Summary}
\label{sec:memsched_summary}
In this chapter, we first analyzed various memory scheduling techniques on SCHPs in the context of cooperative heterogeneous computing, where a single application is partitioned between and concurrently runs on both the CPU and the GPU.
We demonstrated that existing memory scheduling techniques proposed for a multi-tasking scenario are not efficient for cooperative heterogeneous computing.
Our analysis indicates that (i) previous scheduling techniques optimized for a multi-tasking scenario consider fairness among applications as the key concern, which in fact hurts the overall throughput in cooperative heterogeneous computing running a single parallel application;
and (ii) the memory access characteristics and patterns of a single parallel application running on an SCHP are dissimilar from those of multiple applications.
We discover that the conventional FR-FCFS policy, which is believed to be inept in multi-core era, is actually very competent for high-throughput cooperative heterogeneous computing.
Finally, based on our analysis on memory access characteristics and patterns, we proposed a new memory scheduling technique (CHC-MS) optimized for cooperative heterogeneous computing.
CHC-MS considerably enhances row-buffer locality, reduces service latency of CPU requests, and improves overall throughput compared to FR-FCFS, SMS, and FQMS in the context of cooperative heterogeneous computing.

