\chapter{Conclusion}
\label{ch:conclusion}

\section{Thesis Summary}
The silicon integration of the CPU and the GPU as a heterogeneous processor is one of the most significant changes in mainstream processors in the past decade.
This heterogeneous integration introduces several challenges.
From the perspective of power consumption, the CPU and the GPU have separate power domains and thus independent voltage/frequency controls, but share a total power budget.
This requires a coordinated power management strategy to maximize the overall throughput of processors.
From the perspective of memory system, the CPU and the GPU share the memory channels and therefore a heterogeneous-core-aware memory scheduling technique is needed.
In addition, the GPU typically consumes substantially higher memory bandwidth than the CPU and the bandwidth demand is continuously growing.
This ever-increasing bandwidth demand requires the shift to serial interface, which provides high bandwidth with high latency.
A hybrid approach is proposed in this thesis to take the advantage of both serial and parallel interfaces.
This heterogeneous memory system featuring hybrid parallel-serial channels further motivates another study attempting to support a low-overhead page migration for asymmetric memory devices.

First, we demonstrated an optimization technique that combines workload partitioning and power budget allocation between the CPU and the GPU to improve the overall throughput of a heterogeneous processor.
The CPU and the GPU have different performance and power characteristics when varying voltage/frequency levels.
By sweeping the number of CPU cores and GPU SMs, the voltage/frequency levels of the CPU and the GPU, and the percentage of work assigned to the CPU and the GPU, we could find the optimal configuration that can improve the overall throughput significantly.
Furthermore, we developed a runtime dynamic voltage/frequency/core scaling algorithm that can find a near-optimal configuration within a few kernel iterations.

Second, we focused on the cooperative heterogeneous computing scenario in which a single parallel workload is partitioned between and executed on both the CPU and the GPU.
Our analysis showed that, from the perspective of memory scheduling, this scenario exhibits very different memory characteristics with multi-tasking scenario.
In addition, overall throughput is the only key performance metric in cooperative heterogeneous computing scenario, while fairness is equally or even more important in multi-tasking scenario.
Correspondingly, our experiments suggested two previous memory scheduling techniques optimized for multi-tasking are not efficient in cooperative heterogeneous computing.
Finally, we proposed various optimization techniques for memory scheduling based on our analysis on memory access pattern and workload characteristics.
The proposed technique reduces the interference between CPU and GPU requests, enhance the row-buffer locality, and improve the overall throughput.

Third, we looked at the challenges in memory bandwidth scaling due to the limitations of current parallel memory interface.
To employ high-speed serial interface (HSI) to boost the bandwidth while avoiding its latency overhead, we proposed hybrid memory channel architecture comprised of one parallel channel and multiple serial channels.
With a hybrid-channel-aware memory channel partitioning technique, the data of latency-sensitive and bandwidth-sensitive applications can be allocated to the parallel and serial channels, respectively.
As a result, the performance of latency-sensitive applications can be improved since channel partitioning eliminates the interference from bandwidth-consuming applications, while the performance of bandwidth-sensitive applications can also be improved since multiple narrow-fast serial channels provide a much higher bandwidth than current systems.
Moreover, we introduced the typical power management opportunities and techniques for HSI, and provided a detailed comparison of power consumption between HSI and DDRx parallel interface.

Finally, we identified a key challenge for heterogeneous memory systems.
We described several memory architectures that enable a fast, small region and a slow, large region within a single memory device.
The assumption behind this asymmetric design is that frequently-accessed memory pages can be placed in the fast, small region.
Considering the difficulties in developing such a smart page placement mechanism, we tackled the problem from a different perspective.
With the proposed shared row-buffer architecture, we provided a page migration mechanism that can move a page between neighboring banks with very low performance overhead.
This page migration technique can help an asymmetric memory architecture with fast (small) and slow (large) banks achieves $\sim$95\% of the performance of a uniform memory architecture with only fast banks.

\section{Future Work}
Throughout the course of this thesis, several additional research opportunities related to emerging technologies and ideas have been discovered.
This section enumerates the observed research opportunities and directions for further study.

\textbf{Coordinated power management with fine-grain power domain:}
As mentioned in Section~\ref{subsec:heter_arch}, Intel's Haswell processors introduced fully integrated on-chip voltage regulator that can potentially support many independent power domains (e.g., per-core power domain) \cite{intelhaswell}.
This provides much more flexibility for power management since each core (either CPU core or GPU SM) can operate at its most power-efficient voltage/frequency level.
On the other hand, a chip-level power constraint has to be satisfied, indicating the power management policies for all different power domains have to coordinate to achieve a globally optimal decision.
This coordinate power management can be a challenging and exciting research direction for future processors with more heterogeneous cores, each of which is allowed to run at a different voltage/frequency level.

\textbf{Unified memory scheduling for both multi-tasking and cooperative heterogeneous computing scenarios:}
In Chapter~\ref{ch:memsched}, we analyzed the memory characteristics of cooperative heterogeneous computing and the reason why previous memory scheduling techniques designed for multi-tasking scenario performs poorly in cooperative heterogeneous computing scenario.
Although we proposed various memory scheduling techniques specifically optimized for cooperative heterogeneous computing, there is not a unified solution yet that can perform well in both scenarios.
Such a memory scheduling scheme may require a novel memory scheduling algorithm and a new memory controller design.

\textbf{Hybrid channel architecture in the context of hybrid memory cube:}
As briefly mentioned in Section~\ref{subsec:hsi_channel_org}, hybrid memory cube (HMC) from Micron \cite{hmcvlsit, hmcspec} stacks multiple DRAM dies atop of a logic die implementing DRAM control, data routing, error correction, and high-speed interconnects communicating with a processor.
Section~\ref{subsec:hsi_channel_org} introduces one option to employ high-speed serial interface (HSI) as a memory interface that can work with commodity DIMM.
However, HMC architecture enables the tight integration of HSI into a DRAM device (i.e., a cube), giving benefits in latency, bandwidth and power consumption.
Therefore, conducting our hybrid channel architecture with HMC instead of commodity DRAM could be interesting.
In addition, the discussion of HSI features in Section~\ref{subsec:hsi_hsi_features} indicates a latency-bandwidth trade-off.
For example, embedded clocking scheme increases the bandwidth by eliminating clock-to-data skew while introducing additional latency for encoding/decoding.
Hence, even with only serial interfaces, there still can be hybrid interfaces either featuring high bandwidth or low latency, which can be an interesting direction to study.

\textbf{Hybrid channel architecture with dynamic page migration:}
Since GPU applications have their sequential portions running on the CPU, it could be beneficial to migrate memory pages from a serial channel to a parallel channel when execution switches from GPU to CPU.
In Chapter~\ref{ch:hsi}, considering the GPU computation typically dominates the total execution time, we did not evaluate the performance impact of possible page migrations.
However, with the advances in GPU programming environment and the tighter integration of the CPU and the GPU, applications may have many relatively small GPU-accelerated phases, resulting in frequent switch of execution between the CPU and the GPU.
Therefore, a dynamic page migration scheme is worth investigating.
The solution could be different with previous techniques targeting non-unified memory access (NUMA) system, if certain specific program behavior of GPU-accelerated applications can be exploited.

\textbf{Dynamic row-buffer partitioning and bank partitioning:} 
The shared row-buffer architecture proposed in Chapter~\ref{ch:pcm} was employed to support lightweight page migration between neighboring memory banks.
In Section~\ref{subsec:pcm_perf_combine}, we used row-buffer partitioning to adapt to the imbalanced memory accesses between SLC and MLC banks.
In addition, there exists another situation in which dynamic row-buffer partitioning can be applied.
For multi-core processors, previous studies have proposed dynamic bank partitioning, which assigns specific DRAM banks to specific cores (threads) to reduce interference among co-running applications \cite{bankpartitioning, firstbankpartitioning}.
Since different DRAM banks are servicing memory requests from different applications, there could be significant imbalance in memory access rates between neighboring banks.
Therefore, dynamic row-buffer partitioning could be utilized.
It can assign one more row buffer to a busy bank that is servicing a memory-intensive application to improve its performance, while reducing a row buffer from a idle bank that is servicing a non-memory-intensive application will not hurt its performance notably.
Overall, the system performance (e.g., weighted speedup) can be improved.

