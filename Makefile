PAPER = dissertation
TEX = $(wildcard *.tex, sections/*.tex frontmatter/*.tex backmatter/*.tex includes/*.tex)
BIB = $(PAPER).bib
FIGS = $(wildcard figures/*.pdf figures/*.png figures/*.eps graphs/*.pdf graphs/*.png graphs/*.eps)

.PHONY: all clean

$(PAPER).pdf: $(TEX) $(BIB) $(FIGS)
	echo $(FIGS)
	pdflatex --shell-escape $(PAPER)
	bibtex $(PAPER)
	pdflatex --shell-escape $(PAPER)
	pdflatex --shell-escape $(PAPER)
	rm -f *-blx.bib
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.out
	rm -f *.lot
	rm -f *.run.xml
	rm -f *.toc
	rm -f frontmatter/frontmatter.aux

clean:
	rm -f *-blx.bib
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.out
	rm -f *.lot
	rm -f *.run.xml
	rm -f *.toc
	rm -f frontmatter/frontmatter.aux
	rm -f dissertation.pdf
