With aggressive technology scaling, chip manufacturers have been integrating both the CPU and the GPU in a single chip.
The CPU is designed for high single-thread performance, while the GPU is designed for high throughput with massive multi-threading.
Therefore, this heterogeneous integration can improve the overall throughput and energy efficiency as the serial and parallel portions of a workload can be efficiently executed on the CPU and the GPU, respectively.

However, such a single-chip heterogeneous processor (SCHP) also introduces unique challenges in terms of managing resources shared between the CPU and the GPU.
First, the CPU and the GPU share a total chip power budget, while the performance of a processor is often limited by power and thermal constraints.
Therefore, the power budget allocation between the CPU and the GPU can impact the overall throughput.
In this thesis, I demonstrate that joint optimization of workload and power budget partitioning between the CPU and the GPU leads to substantially higher throughput than the optimization of workload partitioning alone under a fixed power budget allocation to the CPU and the GPU.
I also propose an effective runtime algorithm that can determine near-optimal or optimal power budget allocation at runtime.

Second, the CPU and the GPU share the off-chip memory bandwidth, which is also a critical resource and often limits the system performance.
Therefore, a memory scheduling policy should take into account the different memory access characteristics of the CPU and the GPU to properly manage the shared memory channel, in order to maximize the overall throughput of an SCHP.
In this thesis, I perform a detailed analysis of memory access characteristics and propose various optimization techniques for a special yet important computing scenario, in which a single parallel workload is partitioned between and executed on both the CPU and the GPU.

On the other hand, motivated by the heterogeneous integration of processing units, heterogeneous memory systems are also studied in this thesis.
First, recent high-speed serial interface (HSI) demonstrates a much higher bit rate with notably longer latency than current parallel interface.
Therefore, I propose a hybrid memory channel architecture consisting of one low-latency parallel channel and multiple high-bandwidth serial channels.
Coupled with a hybrid-channel-aware memory page mapping technique, this hybrid channel architecture can improve the performance of an SCHP by mapping physical memory pages of latency-sensitive CPU and bandwidth-consuming GPU applications to parallel and serial channels, respectively.

Second, as a typical challenge, heterogeneous memory systems inevitably require a judicious page placement mechanism.
Specifically, an asymmetric memory device with a fast, small region and a slow, large region requires frequently-accessed pages to be placed in the fast region.
Considering the difficulties in implementing such a smart page placement mechanism, I propose a lightweight page migration mechanism that can transfer a page between fast and slow regions through a high-bandwidth, in-memory path, incurring minimal performance overhead.

